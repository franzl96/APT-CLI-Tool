package de.ss21aptcli.presentation.rename;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.commands.rename.RenameCommand;
import de.ss21aptcli.application.commands.rename.RenameCommandData;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.application.interfaces.IVoidCommand;
import de.ss21aptcli.common.LogLevelOut;
import de.ss21aptcli.domain.models.FTPData;
import de.ss21aptcli.infrastructure.FTPConnectionException;
import de.ss21aptcli.infrastructure.FTPDataException;
import de.ss21aptcli.presentation.ServiceMapping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;

import java.util.concurrent.Callable;

/**
 * A class to handel the rename command
 */

@CommandLine.Command(name = "rename", mixinStandardHelpOptions = true, version = "ls 1.0",
        description = "Renames the specified file.")

public class Rename implements Callable<Integer> {

    /**
     *  Initialize LOGGER for Rename class
     */
    private static final Logger LOGGER = LogManager.getLogger(Rename.class);

    @CommandLine.Parameters(index = "0", description = ("The file or directory that should be renamed."))
    private String file;

    @CommandLine.Parameters(index = "0", description = ("The file to be renamed."))
    private String oldName;

    @CommandLine.Parameters(index = "1", description = ("The new name of the file."))
    private String newName;

    @CommandLine.Option(names = {"-pw" , "--password"}, description = "This option set the password for the FTP connection!")
    private String password = "";

    /**
     * Call method for the rename command
     */
    @Override
    public Integer call() {

        try {
            IFileSystem fileSystem;

            // Check if FileSystemImpl or FTPSystemImpl should be used
            if(password.equals("")) {
                fileSystem = ServiceMapping.getFileSystemImpl();
            } else {
                fileSystem = ServiceMapping.getFTPSystemImpl(new FTPData(password, file));
            }

            IVoidCommand<RenameCommandData> rename = new RenameCommand(fileSystem);
            rename.handle(new RenameCommandData(oldName, newName));

        } catch (CommandLineException | FTPDataException | FTPConnectionException e){
            LOGGER.log(LogLevelOut.out(), "File not found: {}", e.getMessage());
        }
        return null;
    }
}
