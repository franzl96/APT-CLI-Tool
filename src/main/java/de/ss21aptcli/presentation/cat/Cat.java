package de.ss21aptcli.presentation.cat;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.stream.Collectors;
import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.commands.cat.CatCommand;
import de.ss21aptcli.application.commands.cat.CatCommandData;
import de.ss21aptcli.application.interfaces.ICommand;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.common.LogLevelOut;
import de.ss21aptcli.domain.models.FTPData;
import de.ss21aptcli.infrastructure.FTPConnectionException;
import de.ss21aptcli.infrastructure.FTPDataException;
import de.ss21aptcli.presentation.ServiceMapping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

/**
 * Reads a specified file and displays its content in the terminal.
 * @author Vitus Henkel, Franz Murner
 * @version 04.06.2021
 */
@Command(name = "cat", mixinStandardHelpOptions = true, version = "cat 1.0",
        description = "Output of the specified file in the terminal.")
public class Cat implements Callable<Integer> {

    private static final Logger LOGGER = LogManager.getLogger(Cat.class.getName());

    @Parameters(index = "0", description = ("The file to be displayed."))
    private String path;

    @CommandLine.Option(names = {"-ln", "--line-numbers"}, description = "Output of the line numbering.")
    private boolean showLineNumbers = false;

    @CommandLine.Option(names = {"-t", "--tail"}, description = "Outputs the last 10 lines.")
    private boolean tail = false;

    @CommandLine.Option(names = {"-pw" , "--password"}, description = "This option set the password for the FTP connection!")
    private String password = "";


    /**
     * Entrypoint in the Command
     * @return The result of the command execution
     */
    @Override
    public Integer call(){

        try {

            IFileSystem fileSystem;
            if (password.equals("")) {
                fileSystem = ServiceMapping.getFileSystemImpl();
            } else {
                fileSystem = getFTPFileSystem();
            }

            ICommand<List<String>, CatCommandData> command = new CatCommand(fileSystem);
            List<String> lines = new ArrayList<>();
            lines = command.handle(new CatCommandData(path));

            formatLines(lines).forEach((line) -> {
                LOGGER.log(LogLevelOut.out(), line);
            });
            return 0;
        }catch (CommandLineException e){
            LOGGER.error(e.getMessage());
            return 1;
        }
    }

    private IFileSystem getFTPFileSystem() throws CommandLineException {
        try {
            FTPData ftpData =   new FTPData(password, path);
            return ServiceMapping.getFTPSystemImpl(ftpData);
        } catch (FTPDataException | FTPConnectionException e) {
            throw new CommandLineException(e.getMessage());
        }
    }

    /**
     * This methode map's simple Options to the lineList and skip anything except the last ten lines (tail)
     * @param lines A list of raw lines
     * @return A list of formatted and filtered lines
     */
    private List<String> formatLines(List<String> lines) {
        return lines.stream()
                .map(new Function<String, String>() {
                    Integer lineNr = 0;
                    @Override
                    public String apply(String s) {
                        if(!showLineNumbers) return s;
                        lineNr++;
                        return lineNr+ ": " + s;
                    }
                })
                .skip(tail ? lines.size() - 1 - 10 : 0).collect(Collectors.toList());
    }

}
