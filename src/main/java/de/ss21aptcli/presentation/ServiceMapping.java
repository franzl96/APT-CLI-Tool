package de.ss21aptcli.presentation;

import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.domain.models.FTPData;
import de.ss21aptcli.infrastructure.FTPConnectionException;
import de.ss21aptcli.infrastructure.LocalFileSystemImpl;
import de.ss21aptcli.infrastructure.FTPFileSystemImpl;

import java.io.PrintStream;

/**
 * Diese Klasse darf auf presentation zugreifen und impls erzeugen.
 */
public class ServiceMapping {

    public static IFileSystem getFileSystemImpl() {
        return new LocalFileSystemImpl();
    }

    public static IFileSystem getFTPSystemImpl(FTPData data) throws FTPConnectionException {
        return new FTPFileSystemImpl(data);
    }

    public static PrintStream getUIOutputStream() {
        return System.out;
    }

}
