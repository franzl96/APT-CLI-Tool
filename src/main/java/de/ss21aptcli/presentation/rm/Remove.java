package de.ss21aptcli.presentation.rm;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.commands.remove.RemoveCommand;
import de.ss21aptcli.application.commands.remove.RemoveCommandData;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.application.interfaces.IVoidCommand;
import de.ss21aptcli.common.LogLevelOut;
import de.ss21aptcli.domain.models.FTPData;
import de.ss21aptcli.infrastructure.FTPConnectionException;
import de.ss21aptcli.infrastructure.FTPDataException;
import de.ss21aptcli.presentation.ServiceMapping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;

import java.util.concurrent.Callable;

/**
 * A class to handle the remove command
 */

@CommandLine.Command(name = "rm", mixinStandardHelpOptions = true, version = "rm 1.0",
        description = "Deletes a file or a directory")

public class Remove implements Callable<Integer> {

    /**
     *  Initialize LOGGER for Remove class
     */
    private final Logger LOGGER = LogManager.getLogger(Remove.class);

    @CommandLine.Parameters(index = "0", description = ("The file or directory that shell be deleted"))
    private String file;

    @CommandLine.Option(names = {"-R", "--recursive"}, description = "Deletes a directory")
    private boolean recursive = false;

    @CommandLine.Option(names = {"-y", "--yes"}, description = "Deletes the directory or file directly without asking again")
    private boolean directly = false;

    @CommandLine.Option(names = {"-pw" , "--password"}, description = "Sets the password for the FTP connection")
    private String password = "";

    @Override
    public Integer call() {

        try {
            IFileSystem fileSystem;

            // Check if FileSystemImpl or FTPSystemImpl should be used
            if(password.equals("")){
                fileSystem = ServiceMapping.getFileSystemImpl();
            } else {
                fileSystem = ServiceMapping.getFTPSystemImpl(new FTPData(password, file));
            }

            IVoidCommand<RemoveCommandData> remove = new RemoveCommand(fileSystem);
            remove.handle(new RemoveCommandData(file, directly));

        } catch (CommandLineException | FTPDataException | FTPConnectionException e){
            LOGGER.log(LogLevelOut.out(), "File could not be deleted: {}", e.getMessage());
        }
        return null;
    }
}
