package de.ss21aptcli.presentation.nano;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.commands.open_file.OpenFileCommand;
import de.ss21aptcli.application.commands.open_file.OpenFileCommandData;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.application.interfaces.IVoidCommand;
import de.ss21aptcli.domain.models.FTPData;
import de.ss21aptcli.infrastructure.FTPConnectionException;
import de.ss21aptcli.infrastructure.FTPDataException;
import de.ss21aptcli.presentation.ServiceMapping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;

import java.io.File;
import java.util.concurrent.Callable;

@CommandLine.Command(name = "nano", mixinStandardHelpOptions = true, version = "ls 1.0",
        description = "Opens the specified file.")

public class Nano implements Callable<Integer> {
    private static final Logger LOGGER = LogManager.getLogger(Nano.class);

    @CommandLine.Parameters(index = "0", description = ("The file to be opened."))
    private File file;

    @CommandLine.Option(names = {"-pw", "--password"}, description = "This option set the password for the FTP connection!")
    private String password = "";

    /*
     * Call function of the nano command
     */
    @Override
    public Integer call() {
        try {
            IFileSystem fileSystem;
            if (password.equals("")) {
                fileSystem = ServiceMapping.getFileSystemImpl();
            } else {
                fileSystem = getFTPFileSystem();
            }
            IVoidCommand<OpenFileCommandData> command = new OpenFileCommand(fileSystem);
            command.handle(new OpenFileCommandData(file));
        } catch (CommandLineException e) {
            LOGGER.error("Die Datei existiert nicht." + e.getMessage());
            return 1;
        }
        return 0;
    }

    private IFileSystem getFTPFileSystem() throws CommandLineException {
        try {
            FTPData ftpData = new FTPData(password, file.getPath());
            return ServiceMapping.getFTPSystemImpl(ftpData);
        } catch (FTPDataException | FTPConnectionException e) {
            throw new CommandLineException(e.getMessage());
        }
    }
}