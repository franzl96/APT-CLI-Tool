package de.ss21aptcli.presentation;

import de.ss21aptcli.common.ArrayToString;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;
import picocli.jansi.graalvm.AnsiConsole;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        int exitCode;
        try (AnsiConsole ansi = AnsiConsole.windowsInstall()) {
            exitCode = new CommandLine(new MainCommand()).execute(args);
        }
        String command = ArrayToString.makeToString(args);
        Level level = Level.forName("COMMAND", 500);
        LOGGER.log(level,"Executed Command {}",command);
        System.exit(exitCode);
    }
}
