package de.ss21aptcli.presentation.log;


import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.commands.log.LogCommand;
import de.ss21aptcli.application.commands.log.LogCommandData;
import de.ss21aptcli.application.interfaces.ICommand;
import de.ss21aptcli.common.LogLevelOut;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * A Class to write to the Log and read the Log
 *
 * @author Alexander Asbeck
 * @version 3.0
 */
@CommandLine.Command(name = "log", mixinStandardHelpOptions = true, version = "log 1.0",
        description = "Prints out the last 50 commands if used without any extra")
public class Log implements Callable<Integer> {

    private static final Logger LOGGER = LogManager.getLogger(Log.class);//Logger for the Class

    @CommandLine.Option(names = {"-e", "--extended"}, description = "Prints out the commands with time and other information")
    private boolean extended = false;

    @CommandLine.Option(names = {"-c", "--command"}, description = "Prints out the logged commands")
    private boolean commands = false;

    @CommandLine.Option(names = {"-E", "--error"}, description = "Prints out the logged errors.")
    private boolean loggedErrors = false;

    @CommandLine.Option(names = {"-i", "--infos"}, description = "Prints out the infos printed to the Log.")
    private boolean infos = false;

    @CommandLine.Option(names = {"-d", "--debug"}, description = "Prints out the logged Debug messages.")
    private boolean loggedDebug = false;

    /**
     * Method is called form picocli and triggers the funktion of this class
     * @return Exit Code
     */
    @Override
    public Integer call() {
        if(!loggedErrors && !loggedDebug && !infos){//In case no argument has been entered only the commands will get displayed
            commands = true;
        }
        ICommand<List<String>, LogCommandData> logCommand = new LogCommand();
        LogCommandData commandData = new LogCommandData(extended,commands,loggedErrors,infos,loggedDebug);
        List<String> output = null;
        try {
            output = logCommand.handle(commandData);
        } catch (CommandLineException e) {
            LOGGER.error(e);
        }
        for (String print: output) {
            LOGGER.log(LogLevelOut.out(), print);
        }
        return 0;
    }

}
