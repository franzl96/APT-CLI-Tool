package de.ss21aptcli.presentation.touch;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.commands.add_new_file.AddNewFileCommand;
import de.ss21aptcli.application.commands.add_new_file.AddNewFileCommandData;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.application.interfaces.IVoidCommand;
import de.ss21aptcli.domain.models.FTPData;
import de.ss21aptcli.infrastructure.FTPConnectionException;
import de.ss21aptcli.infrastructure.FTPDataException;
import de.ss21aptcli.presentation.ServiceMapping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Callable;

/**
 * A class to add a new file
 */
@CommandLine.Command(name = "touch", mixinStandardHelpOptions = true, version = "touch 1.0",
        description = "Create File")

public class FileCreator implements Callable<Integer> {
    private final static Logger LOGGER = LogManager.getLogger(FileCreator.class);

    @CommandLine.Option(names = {"-p", "--path"}, description = "Shows the current Path.")
    private boolean currentPath = false;

    @CommandLine.Parameters(index = "0", description = ("Specify the name of the file and create it."))
    private String newFile;

    @CommandLine.Option(names = {"-pw" , "--password"}, description = "This option set the password for the FTP connection!")
    private String password = "";

    private Path currentRelativePath = Paths.get("");
    private String s = currentRelativePath.toAbsolutePath().toString();
    private String dir = s;

    /**
     * Method call for the touch command
     */

    @Override
    public Integer call() {
        try{
            IFileSystem fileSystem;
            if (password.equals("")) {
                fileSystem = ServiceMapping.getFileSystemImpl();
            } else {
                fileSystem = getFTPFileSystem();
            }
            IVoidCommand<AddNewFileCommandData> command = new AddNewFileCommand(fileSystem);
            command.handle(new AddNewFileCommandData(currentPath, newFile, dir));
        } catch (CommandLineException e) {
            LOGGER.error("Die Datei existiert bereits." + e.getMessage());
            return 1;
        }
        return 0;
    }

    private IFileSystem getFTPFileSystem() throws CommandLineException {
        try {
            FTPData ftpData = new FTPData(password, newFile);
            return ServiceMapping.getFTPSystemImpl(ftpData);
        } catch (FTPDataException | FTPConnectionException e) {
            throw new CommandLineException(e.getMessage());
        }
    }

}