package de.ss21aptcli.presentation;

import de.ss21aptcli.presentation.cat.Cat;
import de.ss21aptcli.presentation.log.Log;
import de.ss21aptcli.presentation.ls.ListDirectory;
import de.ss21aptcli.presentation.rm.Remove;
import de.ss21aptcli.presentation.nano.Nano;
import de.ss21aptcli.presentation.rename.Rename;
import de.ss21aptcli.presentation.mkdir.DirectoryCreator;
import de.ss21aptcli.presentation.touch.FileCreator;
import picocli.CommandLine;
import picocli.CommandLine.Command;

import java.util.concurrent.Callable;

@Command(
        name = "apt-cli",
        description = "Filesystem utility",
        mixinStandardHelpOptions = true,
        version = "1.0",
        subcommands = {
                ListDirectory.class,
                DirectoryCreator.class,
                Log.class,
                Cat.class,
                Nano.class,
                FileCreator.class,
                Rename.class,
                Remove.class
        }
)
public class MainCommand implements Callable<Integer> {
    @Override
    public Integer call() {
        CommandLine.usage(new MainCommand(), System.out);
        return 0;
    }
}
