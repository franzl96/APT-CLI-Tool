package de.ss21aptcli.presentation.ls;

public enum LsColor {
    USER("fg(230)"), // Cornsilk1

    FOLDER("blue"),
    READ_PERMISSION("green"),
    WRITE_PERMISSION("yellow"),
    EXECUTE_PERMISSION("red"),
    NO_PERMISSION("fg(245)"), // Grey

    TIME_MODIFIED_HOUR_OLD("fg(40)"), // Green3
    TIME_MODIFIED_DAY_OLD("fg(42)"), // SpringGreen2
    TIME_MODIFIED_OLDER("fg(36)"), // DarkCyan

    NON_FILE_SIZE("fg(245)"), // Grey
    SMALL_FILE_SIZE("fg(229)"), // Wheat1
    MEDIUM_FILE_SIZE("fg(216)"), // LightSalmon1
    LARGE_FILE_SIZE("fg(172)"); // Orange3

    private final String style;

    LsColor(String style) {
        this.style = style;
    }

    public String format(String input) {
        return String.format("@|" + style + " %s|@", input);
    }
}
