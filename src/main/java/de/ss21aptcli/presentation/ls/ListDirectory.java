package de.ss21aptcli.presentation.ls;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.commands.list_dir.ListDirCommand;
import de.ss21aptcli.application.commands.list_dir.ListDirCommandData;
import de.ss21aptcli.application.interfaces.ICommand;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.domain.models.FTPData;
import de.ss21aptcli.domain.models.FileMeta;
import de.ss21aptcli.infrastructure.FTPConnectionException;
import de.ss21aptcli.infrastructure.FTPDataException;
import de.ss21aptcli.presentation.ServiceMapping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

@Command(name = "ls", mixinStandardHelpOptions = true, version = "ls 1.0.0",
        description = "List information about the FILEs (the current directory by default)")
public class ListDirectory implements Callable<Integer> {
    private final Logger logger = LogManager.getLogger(ListDirectory.class);

    private final DateTimeFormatter lastModifiedDateFormat = DateTimeFormatter.ofPattern("EE MMM dd HH:mm:ss yyyy");

    @Option(names = {"-a", "--all"}, description = "Do not ignore entries starting with .")
    private boolean all = false;

    @Option(names = {"-A", "--almost-all"}, description = "Do not list implied . and ..")
    private boolean almostAll = false;

    @Option(names = {"-l", "--long"}, description = "Display extended file metadata as a table")
    private boolean list = false;

    @Option(names = {"-S", "--size-sort"}, description = "Sort by size")
    private boolean sizeSort = false;

    @Option(names = {"-T", "--time-sort"}, description = "Sort by time modified")
    private boolean timeSort = false;

    @Option(names = {"-D", "--directory-sort"}, description = "Sort directories first")
    private boolean directorySort = false;

    @Option(names = {"-r", "--reverse"}, description = "Reverse the order of the sort")
    private boolean reverseSort = false;

    @Option(names = {"-d", "--directory-only"}, description = "Display directories themselves, and not their contents")
    private boolean directoryOnly = false;

    @Option(names = {"-pw" , "--password"}, description = "This option set the password for the FTP connection!")
    private String password = "";


    /**
     * This variable helps, to get the path. If not path given the result is null.
     */
    @Parameters(arity = "0..1", description = "Filepath to use, default: .")
    private String path = ".";

    @Override
    public Integer call() {
        try {
            ICommand<List<FileMeta>, ListDirCommandData> command = null;
            IFileSystem fileSystem = null;
            if (this.password != "") {
                fileSystem = getFTPFileSystemImpl();
            } else {
                fileSystem = ServiceMapping.getFileSystemImpl();
            }
            command = new ListDirCommand(fileSystem);
            List<FileMeta> contents =
                    command.handle(
                            new ListDirCommandData(
                                    all,
                                    almostAll,
                                    sizeSort,
                                    timeSort,
                                    reverseSort,
                                    directorySort,
                                    directoryOnly,
                                    path));
            printOutput(contents);
            return 0;
        } catch (CommandLineException e) {
            // Sag dem User bescheid, dass das nicht klappt
            logger.error(e.getMessage());
            return 1;
        }
    }

    /**
     * This methode gets the FTPFileSystem implementation
     * @return IFileSystem a service that helps to get the FileMetaData from a specific ftp path
     * @throws CommandLineException Any exception that happening while getting the files
     */
    private IFileSystem getFTPFileSystemImpl() throws CommandLineException {
        try {
            FTPData ftpData = new FTPData(password, path);
            return ServiceMapping.getFTPSystemImpl(ftpData);
        } catch (FTPDataException | FTPConnectionException e) {
            throw new CommandLineException(e.getMessage());
        }
    }

    /** A output method call depends on the flags of the command line
     * @param contents contents of a directory in file meta format
     */
    private void printOutput(List<FileMeta> contents){
        if(!list){
            printOutputAsLine(contents);
        }else{
            printOutputAsList(contents);
        }
    }

    /**
     * A single lined output of filenames
     * @param contents contents of a directory in file meta format
     */
    private void printOutputAsLine(List<FileMeta> contents) {
        String output = contents
                .stream()
                // Apply conditional directory formatting
                .map(f -> f.isDir() ? LsColor.FOLDER.format(f.getName()) : f.getName())
                // Join with spaces
                .collect(Collectors.joining(" "));

        // Apply color formatting
        output = CommandLine.Help.Ansi.AUTO.string(output);
        ServiceMapping.getUIOutputStream().println(output);
    }

    /**
     * A multi-lined output of the content from a directory
     * @param contents contents of a directory in file meta format
     */
    private void printOutputAsList(List<FileMeta> contents){
        new OutputHelper().printFilesAsList(contents);
    }
}