package de.ss21aptcli.presentation.ls;

import de.ss21aptcli.domain.models.FileMeta;
import picocli.CommandLine;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

/**
 * This class has some methods that creates a human readable output on the console!
 */
public class OutputHelper {

    private final DateTimeFormatter lastModifiedDateFormat = DateTimeFormatter.ofPattern("EE MMM dd HH:mm:ss yyyy");

    private int longestOwner = 0;
    private final List<String> allOwners = new LinkedList<>();

    private int longestSize = 0;
    private final List<String> allSizes = new LinkedList<>();

    private int longestSizeSymbol = 0;
    private final List<String> allSizeSymbols = new LinkedList<>();

    /**
     * Showing the files in a list order like the ls -l command
     * @param files A list of file metas
     */
    public void printFilesAsList(List<FileMeta> files) {
        getLongestStringSize(files);

        for (int index = 0; index < files.size(); index++) {
            FileMeta file = files.get(index);

            String typeAndPermissionsFromFile = typeAndPermissionsFromFile(file);
            String owner = getFormattedString(LsColor.USER, longestOwner, allOwners.get(index));
            String fileSizeDescription = fileSizeFormatter(file.getSize(), allSizes.get(index), allSizeSymbols.get(index));
            String formattedDate = formatDate(file.getLastModified());
            String fileName = file.getName();


            if (file.isDir()) fileName = setColor(LsColor.FOLDER, fileName);

            String output = String.format("%s %s %s %s %s", typeAndPermissionsFromFile, owner, fileSizeDescription, formattedDate, fileName);
            System.out.println(CommandLine.Help.Ansi.AUTO.string(output));
        }
    }

    /**
     * Find the longest string for a parameter in the line to create the formatting
     * @param files A list of FileMeta
     */
    private void getLongestStringSize(List<FileMeta> files){
        for (FileMeta file : files) {
            //Owners
            String fileOwner = file.getOwner();
            if (fileOwner != null && fileOwner.length() > longestOwner) {
                longestOwner = fileOwner.length();
            }
            allOwners.add(fileOwner);

            //FileSize
            long sizeInBytes = file.getSize();
            String[] formattedSize = humanReadableByteCountSI(sizeInBytes).split(" ");

            String size = formattedSize[0];
            if (size.length() > longestSize) {
                longestSize = size.length();
            }
            allSizes.add(size);

            //FileSizeSymbol
            String sizeSymbol = formattedSize[1];
            if (sizeSymbol.length() > longestSizeSymbol) {
                longestSizeSymbol = sizeSymbol.length();
            }
            allSizeSymbols.add(sizeSymbol);
        }
    }

    /**
     * Set the right color for the parameter
     * @param lsColor the color format for the text
     * @param text the text itself
     * @return a color formatted String
     */
    private String setColor(LsColor lsColor, String text){
        return lsColor.format(text);
    }

    /**
     * Set the format length for list output to align the text like a table
     * @param maxLength The max length of a cell
     * @return a format for the table
     */
    private String setFormat(Integer maxLength){
        return "%-" + maxLength + "s";
    }

    /**
     * Create the colorized and formatted line
     * @return colorized and formatted line
     */
    private String getFormattedString(LsColor color, Integer maxLength, String text){
        if(maxLength == 0) maxLength = 1; //Length with zero creates an error
        return setColor(color, String.format(setFormat(maxLength), text));
    }

    /**
     * Create the right size color for the description
     * @param fileSize raw size in bytes
     * @param sizeString max cell length
     * @param symbol size symbole
     * @return a color formatted size string
     */
    private String fileSizeFormatter(Long fileSize, String sizeString, String symbol){
        String fileSizeString = String.format(setFormat(longestSize), sizeString);
        String fileSizeSymbolString = String.format(setFormat(longestSizeSymbol), symbol);
        String fileSizeDescription = fileSizeString + fileSizeSymbolString;
        LsColor lsColor;
        if (fileSize >= 1000 * 1000 * 1000) {
            // Larger than 1 GB
            lsColor = LsColor.LARGE_FILE_SIZE;
        } else if (fileSize >= 1000) {
            // Larger than 1 MB
            lsColor = LsColor.MEDIUM_FILE_SIZE;
        } else {
            lsColor = LsColor.SMALL_FILE_SIZE;
        }
        return setColor(lsColor, fileSizeDescription);
    }

    /**
     * Format the date for the output
     * @param date the local date time
     * @return the formatted date string
     */
    private String formatDate(LocalDateTime date) {
        String formattedDate = lastModifiedDateFormat.format(date);

        LocalDateTime now = LocalDateTime.now();
        if (date.isAfter(now.minus(Duration.ofHours(1)))) {
            return LsColor.TIME_MODIFIED_HOUR_OLD.format(formattedDate);
        } else if (date.isAfter(now.minus(Duration.ofDays(1)))) {
            return LsColor.TIME_MODIFIED_DAY_OLD.format(formattedDate);
        } else {
            return LsColor.TIME_MODIFIED_OLDER.format(formattedDate);
        }
    }

    /**
     * Creates from the raw byte length a human readable byte description
     * @param bytes the raw byte length
     * @return the formatted byte description
     */
    private String humanReadableByteCountSI(long bytes) {
        if (-1000 < bytes && bytes < 1000) {
            return bytes + " B";
        }
        CharacterIterator ci = new StringCharacterIterator("kMGTPE");
        while (bytes <= -999_950 || bytes >= 999_950) {
            bytes /= 1000;
            ci.next();
        }
        return String.format("%.1f %cB", bytes / 1000.0, ci.current());
    }

    /**
     * Gets the permissions of a file and show it
     * @param file a file that described by file meta
     * @return a string of permissions
     */
    private String typeAndPermissionsFromFile(FileMeta file) {
        String type;
        if (file.isDir()) {
            type = LsColor.FOLDER.format("d");
        } else {
            type = ".";
        }

        String read;
        if (file.canRead()) {
            read = LsColor.READ_PERMISSION.format("r");
        } else {
            read = LsColor.NO_PERMISSION.format("-");
        }

        String write;
        if (file.canWrite()) {
            write = LsColor.WRITE_PERMISSION.format("w");
        } else {
            write = LsColor.NO_PERMISSION.format("-");
        }

        String execute;
        if (file.canExecute()) {
            execute = LsColor.EXECUTE_PERMISSION.format("x");
        } else {
            execute = LsColor.NO_PERMISSION.format("-");
        }

        return type + read + write + execute;
    }
}
