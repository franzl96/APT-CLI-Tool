package de.ss21aptcli.presentation.mkdir;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.commands.mkdir.MakeDirectoryCommand;
import de.ss21aptcli.application.commands.mkdir.MakeDirectoryCommandData;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.application.interfaces.IVoidCommand;
import de.ss21aptcli.common.LogLevelOut;
import de.ss21aptcli.domain.models.FTPData;
import de.ss21aptcli.infrastructure.FTPConnectionException;
import de.ss21aptcli.infrastructure.FTPDataException;
import de.ss21aptcli.presentation.ServiceMapping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Callable;

/**
 * A class to make a new directory
 */

@CommandLine.Command(name = "mkdir", mixinStandardHelpOptions = true, version = "mkdir 1.0",
        description = "Create Directory")
public class DirectoryCreator implements Callable<Integer> {
    private final static Logger LOGGER = LogManager.getLogger(DirectoryCreator.class);

    @CommandLine.Option(names = {"-p", "--path"}, description = "Shows the current Path.")
    private boolean currentPath = false;

    @CommandLine.Parameters(index = "0", description = ("Specify the name of the directory and create it."))
    private String newDirectory;

    @CommandLine.Option(names = {"-pw", "--password"}, description = "This option set the password for the FTP connection!")
    private String password = "";

    private Path currentRelativePath = Paths.get("");
    private String s = currentRelativePath.toAbsolutePath().toString();
    private String dir = s;

    /**
     * Method call for the make directory command
     */
    @Override
    public Integer call() {
        try {
            IFileSystem fileSystem;
            if (password.equals("")) {
                fileSystem = ServiceMapping.getFileSystemImpl();
            } else {
                fileSystem = getFTPFileSystem();
            }
            IVoidCommand<MakeDirectoryCommandData> command = new MakeDirectoryCommand(fileSystem);
            command.handle(new MakeDirectoryCommandData(currentPath, newDirectory, dir));
        } catch (CommandLineException e) {
            LOGGER.log(LogLevelOut.out(), "Path already exists: " + e);
            return 1;
        }
        return 0;
    }

    private IFileSystem getFTPFileSystem() throws CommandLineException {
        try {
            FTPData ftpData = new FTPData(password, newDirectory);
            return ServiceMapping.getFTPSystemImpl(ftpData);
        } catch (FTPDataException | FTPConnectionException e) {
            throw new CommandLineException(e.getMessage());
        }
    }
}
