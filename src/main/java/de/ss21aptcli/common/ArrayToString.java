package de.ss21aptcli.common;

/**
 * A Class that Converts an Array to a String
 *
 * @author Alexander Asbeck
 * @version 1.1
 */
public class ArrayToString {

    /**
     * Simple Constructor for {@link ArrayToString}
     * Just for creating the Class
     */
    private ArrayToString() {
    }

    /**
     * Turns a Array into a String
     *
     * @param array Array that should be turned into a String
     * @return A String made from the array input
     */
    public static String makeToString(Object[] array) {
        if (array.length == 0)
            return "";
        StringBuilder buffer = new StringBuilder();
        buffer.append(array[0]);
        for (int i = 1; i < array.length; i++) {
            buffer.append(" ").append(array[i]);
        }
        return buffer.toString();
    }
}
