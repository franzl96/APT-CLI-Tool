package de.ss21aptcli.common;

import org.apache.logging.log4j.Level;

/**
 * Class which returns a LogLevel for the Logger
 * @version 1.0
 * @author Alexander
 */
public class LogLevelOut {

    private LogLevelOut(){

    }

    /**
     * Returns the Loglevel for outputs to the commandline
     * @return Loglevel out
     */
    public static Level out(){
        return Level.forName("OUT", 400);
    }

}
