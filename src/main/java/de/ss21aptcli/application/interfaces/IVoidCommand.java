package de.ss21aptcli.application.interfaces;

import de.ss21aptcli.application.exceptions.CommandLineException;

/**
 * Interface for handling any command
 * @param <V> Parameter of the command
 */
public interface IVoidCommand<V> {

    /**
     * Calls the command
     * @param params parameter of the command
     * @throws CommandLineException if any error occurs this exception will be thrown
     */
    void handle(V params) throws CommandLineException;

}
