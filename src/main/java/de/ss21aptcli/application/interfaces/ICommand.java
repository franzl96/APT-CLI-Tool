package de.ss21aptcli.application.interfaces;

import de.ss21aptcli.application.exceptions.CommandLineException;

/**
 * Interface for handling any command
 * @param <T> Return value of the command
 * @param <V> Parameter of the command
 */
public interface ICommand<T, V> {

    /**
     * Calls the command
     * @param params parameter of the command
     * @return return value of the called command
     * @throws CommandLineException if any error occurs this exception will be thrown
     */
    T handle(V params) throws CommandLineException;

}
