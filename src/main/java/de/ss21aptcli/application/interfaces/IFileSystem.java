package de.ss21aptcli.application.interfaces;

import de.ss21aptcli.application.exceptions.*;
import de.ss21aptcli.domain.models.FileMeta;

import java.io.File;
import java.util.List;

/**
 * This interface must be implemented in order to access the file system on a
 * computer.
 */
public interface IFileSystem {

    /**
     * Reads all files and directories in a given path.
     * @param dirPath Path in file system that should be read
     * @return All contents inside the given path
     * @throws PathNotFoundException If path wasn't found
     * @throws FileSystemException Error while interacting with the filesystem
     */
    List<FileMeta> getDirectoryContents(String dirPath) throws PathNotFoundException, FileSystemException;

    /**
     * Returns a file from a given path
     * @param filePath Path to the file that should be returned
     * @return A file from the given path
     * @throws PathNotFoundException If path wasn't found
     * @throws FileNotFound If file wasn't found
     */
    FileMeta getFile(String filePath) throws PathNotFoundException, FileNotFound;

    /**
     * Removes a file or directory
     * @param file file or directory to be removed
     * @throws FileNotFound if the given file or directory doesn't exist
     */
    void removeFile(String file) throws FileNotFound;

    /**
     * Opens an existing file
     * @param file which should be opened
     */
    void openFile(File file) throws DesktopNotSupportedException;

    /**
     * Creates a directory in a given path
     * @param newDirectory New directory that should be created
     * @param directories the directories of the path
     * @param currentPath if this parameter is true the currentPath should be printed
     * @throws PathAlreadyExistsException If path already exists
     */
    void makeDirectory(String newDirectory, String directories, boolean currentPath) throws PathAlreadyExistsException;

    /**
     * Creates a file in a given path
     * @param newFile File that should be created
     * @param directories Path to the file that should created
     * @param currentPath parameter which prints the current working directory if set
     * @throws FileAlreadyExistsException If file already exists
     */
    void createFile(String newFile, String directories, boolean currentPath) throws FileAlreadyExistsException;

    /**
     * Renames a file or directory
     * @param oldName old name of the file or directory
     * @param newName new name of the file or directory
     * @throws FileNotFound if the given file or directory doesn't exist
     */
    void renameFile(String oldName, String newName) throws FileNotFound;

}
