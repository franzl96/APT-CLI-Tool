package de.ss21aptcli.application.commands.log;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.exceptions.FileNotFound;
import de.ss21aptcli.application.exceptions.PathNotFoundException;
import de.ss21aptcli.application.interfaces.ICommand;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.domain.models.FileMeta;
import de.ss21aptcli.infrastructure.LocalFileSystemImpl;
import de.ss21aptcli.presentation.ServiceMapping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * Command for the Log
 * @version 1.0
 * @author Alexander
 */
public class LogCommand implements ICommand<List<String>,LogCommandData> {

    private static final Logger LOGGER = LogManager.getLogger(LogCommand.class);//Logger for the Class
    private static final String EMPTYLOG = "Log is empty";//Sting if the Log is empty
    private static final  String LOGPATH = "logs/log.log";//Path where the log is stored
    private final IFileSystem fileSystem;

    public LogCommand() {
        fileSystem = ServiceMapping.getFileSystemImpl();
    }

    public LogCommand(IFileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    /**
     * Calls the command
     * @param params parameter of the command
     * @return return value of the called command
     * @throws CommandLineException if any error occurs this exception will be thrown
     */
    @Override
    public List<String> handle(LogCommandData params) throws CommandLineException {
        FileMeta file = null;
        try {
            file = fileSystem.getFile(LOGPATH);
        } catch (PathNotFoundException | FileNotFound e) {
            LOGGER.error(e);
            return new ArrayList<>();
        }
        Deque<String> content = new LinkedList<>(file.getContent());
        if(content.isEmpty()){
            return new ArrayList<>();
        }

        List<String> out = new LinkedList<>();
        while (!content.isEmpty()) {
            String next = content.poll();
            if (toAdd(next, params)) {
                out.add(next);
            }
        }

        reduceToSize50(out);
        if(!params.isExtended()){
            getStatusAndMessage(out);
        }
        if(out.isEmpty())
            out.add(EMPTYLOG);
        return out;
    }

    /**
     * A Method to reduce a List to 50 elements
     *
     * @param list A Deque witch shell be transformed into a String
     */
    private void reduceToSize50(List<String> list) {
        while (list.size() > 50) {
            list.remove(0);
        }
    }

    /**
     * <h>Returns the Status and Message from the Log</h>
     * Without the Date and every thing else
     * <p>Note: Only use it for Stings that readFromLog outputs
     *
     * @param list String from readFromLog
     */
    private void getStatusAndMessage(List<String> list) {
        if(list.isEmpty()){
            return;
        }
        for (int i = 0; i < list.size(); i++) {
            StringBuilder stringBuilder = new StringBuilder();
            if(list.get(i).isEmpty())
                continue;
            String temp = list.remove(i);
            LinkedList<String> split= new LinkedList<>(Arrays.asList(temp.split(" ")));
            split.removeFirst();//removes the date from the output
            split.removeFirst();//removes the time from the output
            stringBuilder.append(split.removeFirst());//Adds the Level of the log message to the output
            while (!split.removeFirst().endsWith("]")); //removes the class where it has been called
            while (!split.isEmpty()){// Adds the rest of the line to the output
                stringBuilder.append(" ").append(split.removeFirst());
            }
            list.add(i, stringBuilder.toString());
        }
    }

    /**
     * decides if an entry should be added to the output
     * @param compareTo A string that should be checkt
     * @param params The params it should be compared to
     * @return True if String should be added <p> False if not
     */
    private boolean toAdd(String compareTo, LogCommandData params){
        if(params.isCommands() && messageIsLevel(compareTo, "COMMAND"))
                return true;
        if(params.isInfos() && messageIsLevel(compareTo, "INFO"))
                return true;
        if(params.isLoggedDebug() && messageIsLevel(compareTo, "DEBUG"))
                return true;
        return params.isLoggedErrors() && messageIsLevel(compareTo, "ERROR");
    }

    /**
     * Compares if a message is from a given loglevel
     * @param toCheck String that should be checked
     * @param level Level it should be compared to
     * @return true if level matches false if not
     */
    private boolean messageIsLevel(String toCheck, String level){
        String[] temp = toCheck.split(" ");
        if(temp.length<3)
            return false;
        return temp[2].equals(level);
    }
}
