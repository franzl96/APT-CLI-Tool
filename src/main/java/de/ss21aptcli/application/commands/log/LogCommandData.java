package de.ss21aptcli.application.commands.log;

/**
 * Contains data from the log command
 */
public class LogCommandData {

    private final boolean extended;
    private final boolean commands;
    private final boolean loggedErrors;
    private final boolean infos;
    private final boolean loggedDebug;

    public LogCommandData(boolean extended, boolean commands, boolean loggedErrors, boolean infos, boolean loggedDebug) {
        this.extended = extended;
        this.commands = commands;
        this.loggedErrors = loggedErrors;
        this.infos = infos;
        this.loggedDebug = loggedDebug;
    }

    public boolean isExtended() {
        return extended;
    }

    public boolean isCommands() {
        return commands;
    }

    public boolean isLoggedErrors() {
        return loggedErrors;
    }

    public boolean isInfos() {
        return infos;
    }

    public boolean isLoggedDebug() {
        return loggedDebug;
    }
}
