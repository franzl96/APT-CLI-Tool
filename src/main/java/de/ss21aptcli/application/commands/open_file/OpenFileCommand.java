package de.ss21aptcli.application.commands.open_file;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.exceptions.DesktopNotSupportedException;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.application.interfaces.IVoidCommand;

/**
 * A class for opening a file
 */
public class OpenFileCommand implements IVoidCommand<OpenFileCommandData> {

    private final IFileSystem fileSystem;

    public OpenFileCommand(IFileSystem fileSystem){
        this.fileSystem = fileSystem;
    }

    @Override
    public void handle(OpenFileCommandData params) throws CommandLineException {

        if(this.fileSystem == null) throw new CommandLineException("Kein Filesystem gefunden!");
        try {
            fileSystem.openFile(params.getFile());
        } catch (DesktopNotSupportedException e) {
            throw new CommandLineException("Desktop is not supported.");
        }
    }
}
