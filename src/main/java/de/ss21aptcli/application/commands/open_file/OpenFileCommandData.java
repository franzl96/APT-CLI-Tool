package de.ss21aptcli.application.commands.open_file;

import java.io.File;

/**
 * This Class set the parameters for the command
 */
public class OpenFileCommandData {

    private final File file;

    public OpenFileCommandData(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

}
