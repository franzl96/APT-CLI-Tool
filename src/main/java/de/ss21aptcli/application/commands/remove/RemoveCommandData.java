package de.ss21aptcli.application.commands.remove;

/**
 * Contains data for the remove command
 */

public class RemoveCommandData {

    // file or directory to be removed
    private final String file;
    // removes a file or directory directly without asking again
    private final boolean directly;

    public RemoveCommandData(String file, boolean directly) {
        this.file = file;
        this.directly = directly;
    }

    public String getFile() {
        return file;
    }

    public boolean isDirectly() {
        return directly;
    }
}
