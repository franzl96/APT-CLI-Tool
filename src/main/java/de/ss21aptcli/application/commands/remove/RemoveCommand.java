package de.ss21aptcli.application.commands.remove;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.exceptions.FileNotFound;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.application.interfaces.IVoidCommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

/**
 * A class to handle the remove command
 */

public class RemoveCommand implements IVoidCommand<RemoveCommandData> {

    /**
     *  Initialize LOGGER for RemoveCommand class
     */
    private static final Logger LOGGER = LogManager.getLogger(RemoveCommand.class);

    private final IFileSystem fileSystem;

    public RemoveCommand(IFileSystem fileSystem){
        this.fileSystem = fileSystem;
    }

    @Override
    public void handle(RemoveCommandData params) throws CommandLineException {

        try {
            if(params.isDirectly()) {
                fileSystem.removeFile(params.getFile());
            } else {
                Scanner scanner = new Scanner(System.in);
                LOGGER.info("Do you really want to delete {}? [Y]es for deleting or [N]o to cancel", params.getFile());
                String temp = scanner.next();
                scanner.close();
                if (temp.equalsIgnoreCase("Yes") || temp.equalsIgnoreCase("y")) {
                    fileSystem.removeFile(params.getFile());
                } else {
                    LOGGER.info("Canceled deleting {}", params.getFile());
                }
            }
        } catch (FileNotFound e) {
            throw new CommandLineException("File or Directory not found");
        }
    }
}