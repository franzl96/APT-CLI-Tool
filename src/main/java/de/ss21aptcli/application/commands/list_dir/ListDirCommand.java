package de.ss21aptcli.application.commands.list_dir;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.exceptions.FileSystemException;
import de.ss21aptcli.application.exceptions.PathNotFoundException;
import de.ss21aptcli.application.interfaces.ICommand;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.domain.models.FileMeta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A class that get a IFileSystem, extract a list of the files in file system, sort and filter them
 */
public class ListDirCommand implements ICommand<List<FileMeta>, ListDirCommandData> {

    private final IFileSystem fileSystem;

    public ListDirCommand(IFileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    public ListDirCommand(){
        this.fileSystem = null;
    }

    public List<FileMeta> handle(ListDirCommandData in) throws CommandLineException {
        if(this.fileSystem == null) throw new CommandLineException("Kein Filesystem gefunden!");
        try {
            List<FileMeta> contents = this.fileSystem.getDirectoryContents(in.getPath());
            return modifyList(contents, in);
        } catch (PathNotFoundException e) {
            throw new CommandLineException("Path " + in.getPath() + " doesn't exists!");
        } catch (FileSystemException e) {
            throw new CommandLineException(e.getMessage());
        }
    }

    public List<FileMeta> modifyList(List<FileMeta> contents, ListDirCommandData in){
        contents = ignoreEntriesWithDot(contents, !in.isAll() && !in.isAlmostAll());
        contents = listAlmostAll(contents, in.isAlmostAll());
        contents = onlyDirs(contents, in.isDirectoryOnly());
        contents = sortBy(contents, in);
        contents = sortDirectoryFirst(contents, in.isDirectorySort());
        contents = reverseList(contents, in.isReverseSort());
        return contents;
    }

    /**
     * You can sort the List only in time or in size
     * @param current a list of files
     * @param in the parameterlist
     * @return A sorted list accordingly by the given parameters
     */
    private List<FileMeta> sortBy(List<FileMeta> current, ListDirCommandData in){
        if(in.isTimeSort()){
            return sortByTime(current);
        }else if(in.isSizeSort()){
            return sortBySize(current);
        }

        return current;
    }

    /**
     * A filter that shows only directories
     * @param current a list of files
     * @param sortOut a boolean value that says if the operation should make
     * @return A sorted list accordingly by the given parameters
     */
    private List<FileMeta> onlyDirs(List<FileMeta> current, boolean sortOut) {
        if (!sortOut) {
            return current;
        }
        List<FileMeta> sortedList = new ArrayList<>();
        for (FileMeta f : current) {
            if (f.isDir()) {
                sortedList.add(f);
            }
        }
        return sortedList;
    }

    /**
     * This methode remove the hidden dot files if needed
     * @param current a list of files
     * @param ignore a boolean value that says if the operation should make
     * @return A sorted list accordingly by the given parameters
     */
    private List<FileMeta> ignoreEntriesWithDot(List<FileMeta> current, boolean ignore) {
        if (!ignore) {
            return current;
        }
        List<FileMeta> sortedList = new ArrayList<>();
        for (FileMeta f : current) {
            if (!f.getName().startsWith(".")) {
                sortedList.add(f);
            }
        }
        return sortedList;
    }

    /**
     * This methode remove the . (Current Directory) and the .. (Parent Directory) from the list
     * @param current a list of files
     * @param value a boolean value that says if the operation should make
     * @return A sorted list accordingly by the given parameters
     */
    private List<FileMeta> listAlmostAll(List<FileMeta> current, boolean value) {
        if (!value) {
            return current;
        }
        List<FileMeta> sortedList = new ArrayList<>();
        for (FileMeta f : current) {
            if (!(f.getName().equals("..") || f.getName().equals("."))){
                sortedList.add(f);
            }
        }
        return sortedList;
    }

    /**
     * This methode sort the files in ascending order by size
     * @param current a list of files
     * @return A sorted list accordingly by the given parameters
     */
    private List<FileMeta> sortBySize(List<FileMeta> current) {

        current.sort(new Comparator<FileMeta>() {
            @Override
            public int compare(FileMeta o1, FileMeta o2) {
                return Long.compare(o2.getSize(), o1.getSize());
            }
        });

        return current;
    }

    /**
     * This methode sort the files in ascending order by size
     * @param current a list of files
     * @return A sorted list accordingly by the given parameters
     */
    private List<FileMeta> sortByTime(List<FileMeta> current){
        current.sort(Comparator.comparing(FileMeta::getLastModified));

        return current;
    }

    /**
     * This methode sorts the directory's on the top
     * @param current a list of files
     * @return A sorted list accordingly by the given parameters
     */
    private List<FileMeta> sortDirectoryFirst(List<FileMeta> current, boolean value){
        if(!value){
            return current;
        }
        current.sort((o1, o2) -> {
            if(o1.isDir()){
                return -1;
            }else if (!o1.isDir()){
                return  1;
            }
            return 0;
        });
        return current;
    }

    private List<FileMeta> reverseList(List<FileMeta> current, boolean value){
        if(!value){
            return current;
        }

        Collections.reverse(current);

        return current;
    }
}
