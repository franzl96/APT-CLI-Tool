package de.ss21aptcli.application.commands.list_dir;

public class ListDirCommandData {

    private final boolean all;
    private final boolean almostAll;
    private final boolean sizeSort;
    private final boolean timeSort;
    private final boolean reverseSort;
    private final boolean directorySort;
    private final boolean directoryOnly;
    private final String path;

    public ListDirCommandData(
            boolean all,
            boolean almostAll,
            boolean sizeSort,
            boolean timeSort,
            boolean reverseSort,
            boolean directorySort,
            boolean directoryOnly,
            String path) {
        this.all = all;
        this.almostAll = almostAll;
        this.sizeSort = sizeSort;
        this.timeSort = timeSort;
        this.reverseSort = reverseSort;
        this.directorySort = directorySort;
        this.directoryOnly = directoryOnly;
        this.path = path;
    }

    public boolean isValid() {
        //
        return true;
    }

    public boolean isAll() {
        return all;
    }

    public boolean isAlmostAll() {
        return almostAll;
    }

    public boolean isSizeSort() {
        return sizeSort;
    }

    public boolean isTimeSort() {
        return timeSort;
    }

    public boolean isReverseSort() {
        return reverseSort;
    }

    public boolean isDirectorySort() {
        return directorySort;
    }

    public boolean isDirectoryOnly() {
        return directoryOnly;
    }

    public String getPath() {
        return path;
    }
}
