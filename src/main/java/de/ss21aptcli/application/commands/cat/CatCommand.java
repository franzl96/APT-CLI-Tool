package de.ss21aptcli.application.commands.cat;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.exceptions.FileNotFound;
import de.ss21aptcli.application.exceptions.PathNotFoundException;
import de.ss21aptcli.application.interfaces.ICommand;
import de.ss21aptcli.application.interfaces.IFileSystem;

import java.util.List;

/**
 * This Class get the lines of a File and create a list of them
 */
public class CatCommand implements ICommand<List<String>, CatCommandData> {


    private final IFileSystem fileSystem;

    public CatCommand(IFileSystem fileSystem){
        this.fileSystem = fileSystem;
    }


    @Override
    public List<String> handle(CatCommandData params) throws CommandLineException {
        try {
            return fileSystem.getFile(params.getPath()).getContent();
        } catch (PathNotFoundException | FileNotFound e) {
            throw new CommandLineException(e.getMessage());
        }
    }
}
