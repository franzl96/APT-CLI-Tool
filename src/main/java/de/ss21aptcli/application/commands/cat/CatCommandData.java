package de.ss21aptcli.application.commands.cat;

/**
 * This Class set the parameters for the command
 */
public class CatCommandData {

    private String path;

    public CatCommandData(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
