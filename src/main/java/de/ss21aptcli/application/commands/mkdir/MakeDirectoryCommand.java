package de.ss21aptcli.application.commands.mkdir;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.exceptions.PathAlreadyExistsException;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.application.interfaces.IVoidCommand;

/**
 * A class for making new directories
 */
public class MakeDirectoryCommand implements IVoidCommand<MakeDirectoryCommandData> {

    private final IFileSystem fileSystem;

    public MakeDirectoryCommand(IFileSystem fileSystem){
        this.fileSystem = fileSystem;
    }

    @Override
    public void handle(MakeDirectoryCommandData params) throws CommandLineException{
        if(this.fileSystem == null) throw new CommandLineException("Kein Filesystem gefunden!");
        try {
            fileSystem.makeDirectory(params.getNewDirectory(), params.getDirectories(), params.isCurrentPath());
        } catch (PathAlreadyExistsException e) {
            throw new CommandLineException("Path " + params.getNewDirectory() + " not exists!");
        }
    }
}
