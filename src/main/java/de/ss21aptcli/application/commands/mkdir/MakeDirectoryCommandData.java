package de.ss21aptcli.application.commands.mkdir;

/**
 * This Class set the parameters for the command
 */
public class MakeDirectoryCommandData {

    private final boolean currentPath;
    private final String newDirectory;
    private final String directories;

    public MakeDirectoryCommandData(boolean currentPath, String newDirectory, String directories) {
        this.currentPath = currentPath;
        this.newDirectory = newDirectory;
        this.directories = directories;
    }

    public boolean isCurrentPath() {
        return currentPath;
    }

    public String getNewDirectory() {
        return newDirectory;
    }

    public String getDirectories() { return directories; }
}
