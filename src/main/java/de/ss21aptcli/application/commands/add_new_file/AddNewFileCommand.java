package de.ss21aptcli.application.commands.add_new_file;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.exceptions.FileAlreadyExistsException;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.application.interfaces.IVoidCommand;

/**
 * A class for adding new Files
 */

public class AddNewFileCommand implements IVoidCommand<AddNewFileCommandData> {

    private final IFileSystem fileSystem;

    public AddNewFileCommand(IFileSystem fileSystem){
        this.fileSystem = fileSystem;
    }

    @Override
    public void handle(AddNewFileCommandData params) throws CommandLineException {
        if(this.fileSystem == null) throw new CommandLineException("Kein Filesystem gefunden!");
        try{ fileSystem.createFile(params.getNewFile(), params.getDirectories(), params.isCurrentPath());
        } catch (FileAlreadyExistsException e){
            throw new CommandLineException("File " + params.getNewFile() + " already exists!");
        }
    }
}
