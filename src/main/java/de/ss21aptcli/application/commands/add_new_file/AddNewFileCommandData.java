package de.ss21aptcli.application.commands.add_new_file;

/**
 * This Class set the parameters for the command
 */
public class AddNewFileCommandData {

    private final boolean currentPath;
    private final String newFile;
    private final String directories;

    public AddNewFileCommandData(boolean currentPath, String newFile, String directories) {
        this.currentPath = currentPath;
        this.newFile = newFile;
        this.directories = directories;
    }

    public boolean isCurrentPath() {
        return currentPath;
    }

    public String getNewFile() {
        return newFile;
    }

    public String getDirectories() {
        return directories;
    }

}
