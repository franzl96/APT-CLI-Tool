package de.ss21aptcli.application.commands.rename;

/**
 * Contains data for the rename command
 */

public class RenameCommandData {

    // old name of the file or directory
    private final String oldName;
    // new name of the file or directory
    private final String newName;

    public RenameCommandData(String oldName, String newName) {
        this.oldName = oldName;
        this.newName = newName;
    }

    public String getOldName() {
        return oldName;
    }

    public String  getNewName() {
        return newName;
    }
}