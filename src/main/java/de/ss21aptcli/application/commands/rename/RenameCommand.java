package de.ss21aptcli.application.commands.rename;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.exceptions.FileNotFound;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.application.interfaces.IVoidCommand;

/**
 * A class to handle the rename command
 */

public class RenameCommand implements IVoidCommand<RenameCommandData> {

    private final IFileSystem fileSystem;

    public RenameCommand(IFileSystem fileSystem){
        this.fileSystem = fileSystem;
    }

    @Override
    public void handle(RenameCommandData params) throws CommandLineException {

        try {
            fileSystem.renameFile(params.getOldName(), params.getNewName());

        } catch (FileNotFound e) {
            throw new CommandLineException("File not found!");
        }
    }
}
