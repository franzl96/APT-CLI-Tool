package de.ss21aptcli.application.exceptions;

public class CommandLineException extends Exception {

    public CommandLineException(String message) {
        super(message);
    }

}
