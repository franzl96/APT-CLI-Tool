package de.ss21aptcli.application.exceptions;

public class DesktopNotSupportedException extends Exception{
    public DesktopNotSupportedException(String message, Throwable inner) {
        super(message, inner);
    }

    public DesktopNotSupportedException(String message) {
        super(message);
    }

}
