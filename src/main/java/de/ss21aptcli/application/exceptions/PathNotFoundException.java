package de.ss21aptcli.application.exceptions;

public class PathNotFoundException extends Exception {

    public PathNotFoundException(String message, Throwable inner) {
        super(message, inner);
    }

}
