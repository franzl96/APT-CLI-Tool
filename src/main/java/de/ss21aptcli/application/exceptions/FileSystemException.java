package de.ss21aptcli.application.exceptions;

public class FileSystemException extends Exception {

    public FileSystemException(String message, Throwable inner) {
        super(message, inner);
    }

    public FileSystemException(Throwable inner) {
        super(inner);
    }
}
