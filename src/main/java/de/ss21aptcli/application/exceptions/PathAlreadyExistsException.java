package de.ss21aptcli.application.exceptions;

public class PathAlreadyExistsException extends Exception {

    public PathAlreadyExistsException(String message, Throwable inner) {
        super(message, inner);
    }
}
