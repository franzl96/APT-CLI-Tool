package de.ss21aptcli.application.exceptions;

public class FileAlreadyExistsException extends Exception {

    public FileAlreadyExistsException(String message, Throwable inner) {
        super(message, inner);
    }

}
