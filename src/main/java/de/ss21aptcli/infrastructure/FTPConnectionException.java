package de.ss21aptcli.infrastructure;

public class FTPConnectionException extends Exception{
    public FTPConnectionException(String msg, Throwable inner){
        super(msg, inner);
    }
}
