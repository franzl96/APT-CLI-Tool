package de.ss21aptcli.infrastructure;

import de.ss21aptcli.application.exceptions.DesktopNotSupportedException;
import de.ss21aptcli.application.exceptions.FileAlreadyExistsException;
import de.ss21aptcli.application.exceptions.FileNotFound;
import de.ss21aptcli.application.exceptions.FileSystemException;
import de.ss21aptcli.application.exceptions.PathAlreadyExistsException;
import de.ss21aptcli.application.exceptions.PathNotFoundException;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.common.LogLevelOut;
import de.ss21aptcli.domain.models.FileMeta;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileOwnerAttributeView;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Gets files of your file system
 */
public class LocalFileSystemImpl implements IFileSystem {

    private final static Logger LOGGER = LogManager.getLogger(LocalFileSystemImpl.class);

    /**
     * Load the files of an directory of your filesystem
     * @param dirPath Path in file system that should be read
     * @return A list of FileMetas
     * @throws PathNotFoundException If the path could not found
     * @throws FileSystemException If any error occurs in the communication with the file system
     */
    @Override
    public List<FileMeta> getDirectoryContents(String dirPath) throws PathNotFoundException, FileSystemException {
        try {
            List<Path> contents = Files.list(Paths.get(dirPath)).collect(Collectors.toList());
            List<FileMeta> fileMeta = new ArrayList<>();

            if(!contents.isEmpty()){
                fileMeta = getDotDirs(contents.get(0));
            }

            for(Path p : contents) {
                fileMeta.add(LocalFileSystemImpl.mapFromPath(p));
            }

            return fileMeta;
        } catch (IOException e) {
            throw new PathNotFoundException("Any exception happening while getting the contents of a file (path wrong?)", e);
        }
    }

    /**
     * Returns a file from a given path
     *
     * @param filePath Path to the file that should be returned
     * @return A file from the given path
     */
    @Override
    public FileMeta getFile(String filePath) throws PathNotFoundException, FileNotFound {
        FileMeta fileMeta = null;
        try {
             fileMeta = mapFromPath(Paths.get(filePath));
        } catch (FileSystemException e) {
            LOGGER.error(e);
        }
        if(fileMeta == null){
            throw new FileNotFound("Path couldn't be found");
        }
        if(fileMeta.isDir()){
            throw new FileNotFound("Path is a directory!");
        }
        return fileMeta;
    }

    /**
     * Removes a file or directory
     * @param file file or directory to be removed
     * @throws FileNotFound if the given file or directory doesn't exist
     */
    @Override
    public void removeFile(String file) throws FileNotFound {
        File stringToFile = new File(file);

        if (stringToFile.isFile()) {
                FileUtils.deleteQuietly(stringToFile);
                LOGGER.log(LogLevelOut.out(), "File {} has been deleted", file);
        } else if (stringToFile.isDirectory()) {
            try {
                FileUtils.deleteDirectory(stringToFile);
                LOGGER.log(LogLevelOut.out(), "Directory {} has been deleted", file);
            } catch (IOException e){
                LOGGER.log(LogLevelOut.out(), "Error: Directory could not be deleted, due to: {}", e.getMessage());
            }
        } else {
            throw new FileNotFound("File or Directory not found");
        }
    }

    /**
     * Renames a file or directory
     * @param from old name of the file or directory
     * @param to new name of the file or directory
     * @throws FileNotFound if the given file or directory doesn't exist
     */
    @Override
    public void renameFile(String from, String to) throws FileNotFound {
        try{
            File oldName = new File(from);
            File newName = new File(to);
            if(oldName.renameTo(newName)) {
                LOGGER.log(LogLevelOut.out(), "{} renamed to: {}", oldName, newName);
            } else {
                LOGGER.log(LogLevelOut.out(), "Error: {} not found!", oldName);
            }
        } catch (Exception e) {
            throw new FileNotFound("File not found!");
        }
    }

    @Override
    public void openFile(File file) throws DesktopNotSupportedException {
        try {
            Desktop desktop = Desktop.getDesktop();
            desktop.open(file);
        } catch (IOException e) {
            throw new DesktopNotSupportedException("Desktop is not supported.",e);
        }
    }

    @Override
    public void makeDirectory(String newDirectory, String directories, boolean currentPath) throws PathAlreadyExistsException {
        if(currentPath){
          LOGGER.log(LogLevelOut.out(), "The current path is: " + directories);
        }
        else {
            File file = new File(directories);
            Path path = Paths.get(String.valueOf(file));
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                throw new PathAlreadyExistsException("Path already exists.", e);
            }
            File newFile = new File(newDirectory);
            newFile.mkdir();
        }
    }

    @Override
    public void createFile(String newFile, String directories, boolean currentPath) throws FileAlreadyExistsException{
        if(currentPath){
            LOGGER.log(LogLevelOut.out(),"The current path is: " + directories);
        }
        else {
            File file = new File(directories);
            Path path = Paths.get(String.valueOf(file));
            try {
                Files.createDirectories(path);
                File newFiletoCreate = new File(newFile);
                newFiletoCreate.createNewFile();
            } catch (IOException e) {
                throw new FileAlreadyExistsException("Path already exists.", e);
            }
        }
    }

    /**
     * Get the current and the parent directory's
     * @param path The path from the command line to the file on the system
     * @return The current directory and if available the parent directory
     * @throws IOException Common Error
     * @throws FileSystemException Error while getting the contents of a file
     */
    private List<FileMeta> getDotDirs(Path path) throws IOException, FileSystemException {
        File parentDotFile = path.toFile().getCanonicalFile().getParentFile();
        File parentDotDotFile = null;

        List<FileMeta> fileMeta = new ArrayList<>();

        if(parentDotFile != null){
            System.out.println(parentDotFile.getAbsolutePath());

            FileMeta fileMetaDot = mapFromPath(parentDotFile.toPath());
            fileMetaDot.setName(".");
            fileMeta.add(fileMetaDot);

            parentDotDotFile = parentDotFile.getParentFile();
        }

        if(parentDotDotFile != null){
            System.out.println(parentDotDotFile.getAbsolutePath());

            FileMeta fileMetaDotDot = mapFromPath(parentDotDotFile.toPath());
            fileMetaDotDot.setName("..");
            fileMeta.add(fileMetaDotDot);
        }

        return fileMeta;
    }

    /**
     * Mapping the file from the file system to FileMeta
     * @param path The path to the file in the file system
     * @return A FileMeta
     * @throws FileSystemException Is thrown if there a problem with the path
     */
    private static FileMeta mapFromPath(Path path) throws FileSystemException{
        try {
            File file = path.toFile();
            FileMeta fileMeta = new FileMeta();
            fileMeta.setDir(file.isDirectory());
            if (!file.isDirectory()) {
                fileMeta.setContent(ReadFile.readFile(file));
            }
            FileOwnerAttributeView fileAttributeView = Files.getFileAttributeView(path, FileOwnerAttributeView.class);
            fileMeta.setOwner(fileAttributeView.getOwner().getName());
            fileMeta.setLastModified(
                    Instant.ofEpochMilli(file.lastModified()).atZone(ZoneId.systemDefault()).toLocalDateTime());
            fileMeta.setSize(file.length());
            fileMeta.setName(file.getName());
            fileMeta.setCanRead(file.canRead());
            fileMeta.setCanWrite(file.canWrite());
            fileMeta.setCanExecute(file.canExecute());

            return fileMeta;
        } catch (IOException e) {
            throw new FileSystemException("Es ist ein unbekannter Fehler aufgetreten...", e);
        }

    }
}
