package de.ss21aptcli.infrastructure;

import de.ss21aptcli.application.exceptions.*;
import de.ss21aptcli.infrastructure.ftpcommands.Connection;
import de.ss21aptcli.infrastructure.ftpcommands.FileUtil;
import de.ss21aptcli.infrastructure.ftpcommands.FtpClient;
import de.ss21aptcli.application.interfaces.IFileSystem;
import de.ss21aptcli.domain.models.FTPData;
import de.ss21aptcli.domain.models.FileMeta;
import org.apache.commons.net.ftp.FTPFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

/**
 * Connects with the FTPServer and collect files in a Filesystem
 */
public class FTPFileSystemImpl implements IFileSystem {

    private final FtpClient ftpClient;

    public FTPFileSystemImpl(FTPData ftpData) throws FTPConnectionException {
        try {
            ftpClient = Connection.connect(ftpData);
        } catch (IOException e) {
            throw new FTPConnectionException("Error while connecting to the ftp server", e);
        }
    }

    /**
     * Load the files of an directory of a ftp server
     *
     * @param dirPath Path in file system that should be read
     * @return A list of FileMetas
     * @throws PathNotFoundException If the path could not found
     * @throws FileSystemException   If any error occurs in the communication with the file system
     */
    @Override
    public List<FileMeta> getDirectoryContents(String dirPath) throws PathNotFoundException, FileSystemException {
        try {
            List<FileMeta> fileMetaList = getDotDirs(dirPath);
            List<FTPFile> files = ftpClient.listFilesAsListOfFTPFiles(dirPath);

            for (FTPFile file : files) {
                fileMetaList.add(mapToFileMeta(file, dirPath));
            }

            ftpClient.close();
            return fileMetaList;
        } catch (IOException e) {
            throw new FileSystemException("An exception occurs while disconnecting the ftp-client", e);
        } catch (FileNotFound fileNotFound) {
            throw new PathNotFoundException("The path not exists!", fileNotFound);
        } catch (FTPDataException e){
            throw new FileSystemException(e);
        }
    }

    /**
     * Returns a file for a given path
     *
     * @param filePath Path to the file that should be returned
     * @throws PathNotFoundException Is thrown if there a problem with the FTP-Path
     * @throws FileNotFound Is thrown if an IOException happened
     * @return A file from the given path
     */
    @Override
    public FileMeta getFile(String filePath) throws PathNotFoundException, FileNotFound {
        try {
            FileMeta fileMeta = getFileMeta(filePath);
            ftpClient.close();
            return fileMeta;
        } catch (FTPDataException e) {
            throw new PathNotFoundException("The FTP path contains wrong syntax", e);
        } catch (IOException e) {
            throw new FileNotFound("A common error while getting the file is happened");
        }
    }

    /**
     * Removes a file or directory
     * @param file file or directory to be removed
     * @throws FileNotFound if the given file or directory doesn't exist
     */
    @Override
    public void removeFile(String file) throws FileNotFound {

        try {
            ftpClient.removeFile(file);
            ftpClient.removeDirectory(file);
        } catch (FTPDataException | IOException e) {
            throw new FileNotFound("Error: File or Directory could not be deleted");
        }
    }

    /**
     * Renames a file or directory
     * @param from old name of the file or directory
     * @param to new name of the file or directory
     * @throws FileNotFound if the given file or directory doesn't exist
     */
    @Override
    public void renameFile(String from, String to) throws FileNotFound {
        try {
            ftpClient.rename(from, to);
        } catch (IOException | FTPDataException e) {
            e.printStackTrace();
            throw new FileNotFound("File not found!");
        }
    }

    /**
     * Get the current and the parent directory's
     * @param ftpPath The ftp path from the command line
     * @return The current directory and if available the parent directory
     * @throws IOException Common Error
     * @throws FileNotFound Thrown if the file not found at the ftp-file-system
     * @throws FTPDataException  Thrown if the path of the FTP command wrong
     */
    private List<FileMeta> getDotDirs(String ftpPath) throws IOException, FileNotFound, FTPDataException {
        List<FileMeta> fileMeta = new ArrayList<>();

        Path path = Path.of(FileUtil.getParsedPath(ftpPath));

        FileMeta file = getFileMeta(ftpPath);
        file.setName(".");
        fileMeta.add(file);

        Path parentPath = path.getParent();

        if (parentPath != null) {
            FileMeta parentFile = getFileMeta(parentPath.toString());
            parentFile.setName("..");
            fileMeta.add(parentFile);
        }

        return fileMeta;
    }

    /**
     * Gets the FileMeta from a Single ftpPath
     * @param ftpPath The ftp path from the command line
     * @return FileMeta Object
     * @throws FTPDataException Is thrown if there a problem with the FTP-Path
     * @throws IOException Is thrown if a error while fetching the file is happened
     */
    private FileMeta getFileMeta(String ftpPath) throws FTPDataException, IOException {
        return mapToFileMeta(ftpClient.mlistFile(ftpPath), ftpPath);
    }

    /**
     * Mapping the FTP-File to the FileMeta
     * @param ftpFile A FTP-File
     * @param ftpPath The ftp path from the command line
     * @return A FileMeta
     * @throws IOException Is thrown if a error happened while get the file content
     * @throws FTPDataException Is thrown if there a problem with the FTP-Path
     */
    private FileMeta mapToFileMeta(FTPFile ftpFile, String ftpPath) throws IOException, FTPDataException {
        FileMeta fileMeta = new FileMeta();
        fileMeta.setDir(ftpFile.isDirectory());

        if (!ftpFile.isDirectory()){
            fileMeta.setContent(ftpClient.getFileContents(ftpPath, ftpFile.getName()));
        }

        fileMeta.setOwner(ftpFile.getUser());
        fileMeta.setLastModified(
                Instant.ofEpochMilli(ftpFile.getTimestamp().getTimeInMillis()).atZone(ZoneId.systemDefault()).toLocalDateTime());
        fileMeta.setSize(ftpFile.getSize());
        fileMeta.setName(ftpFile.getName());
        fileMeta.setCanRead(ftpFile.hasPermission(FTPFile.WORLD_ACCESS, FTPFile.READ_PERMISSION));
        fileMeta.setCanWrite(ftpFile.hasPermission(FTPFile.WORLD_ACCESS, FTPFile.WRITE_PERMISSION));
        fileMeta.setCanExecute(ftpFile.hasPermission(FTPFile.WORLD_ACCESS, FTPFile.EXECUTE_PERMISSION));

        return fileMeta;
    }

    @Override
    public void openFile(File file) throws DesktopNotSupportedException {
        throw new DesktopNotSupportedException("FTPClient doesn't support opening Files.");
    }

    @Override
    public void makeDirectory(String newDirectory, String directories, boolean currentPath) throws PathAlreadyExistsException {
        try {
            ftpClient.makeDirectory(newDirectory);
        } catch( IOException | FTPDataException e) {
            throw new PathAlreadyExistsException("Path already exists.", e);
        }
    }

    @Override
    public void createFile(String newFile, String directories, boolean currentPath) throws FileAlreadyExistsException{
        try {
            ftpClient.touch(newFile);
        } catch(FTPDataException | IOException e) {
            throw new FileAlreadyExistsException("File already exits.", e);
        }
    }
}
