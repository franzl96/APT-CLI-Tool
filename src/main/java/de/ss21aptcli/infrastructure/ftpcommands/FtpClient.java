package de.ss21aptcli.infrastructure.ftpcommands;

import de.ss21aptcli.infrastructure.FTPDataException;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Inet4Address;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class FtpClient {

    private static final Logger LOGGER = LogManager.getLogger(FtpClient.class);

    private final String server;
    private final int port;

    private final String user;
    private final String password;
    private final Inet4Address inetAdress;
    private FTPClient ftp;

    private static final String TEMP_FOLDER ="./temp83907654";


    FtpClient(String server, int port, String user, String password) {
        this.server = server;
        this.port = port;
        this.user = user;
        this.password = password;
        this.inetAdress = null;
    }

    FtpClient(int port, String user, String password, Inet4Address inetAdress) {
        this.server = "";
        this.port = port;
        this.user = user;
        this.password = password;
        this.inetAdress= inetAdress;

    }


    void openWithInetAdress() throws IOException {

        ftp = new FTPClient();
        ftp.enterLocalPassiveMode();
        ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
        ftp.connect(inetAdress, port);
        int reply = ftp.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            ftp.disconnect();
            throw new IOException("Exception in connecting to FTP Server");
        }
        ftp.login(user, password);

    }

    void open() throws IOException {
        ftp = new FTPClient();
        ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
        ftp.connect(server, port);

        int reply = ftp.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            ftp.disconnect();
            throw new IOException("Exception in connecting to FTP Server");
        }

        ftp.login(user, password);
        ftp.enterLocalPassiveMode();

    }

    public void close() throws IOException {
        ftp.disconnect();
    }

    //ls on root
    public Collection<String> listFiles() throws IOException, FTPDataException {
        return listFiles("");
    }

    //ls
    public List<FTPFile> listFilesAsListOfFTPFiles(String path) throws IOException, FTPDataException {
        path=FileUtil.getParsedPath(path);
        if(path.equals("")){
            path="/";
        }
        FTPFile[] files = ftp.listFiles(path);
        for(FTPFile file: files){
            LOGGER.debug(file);
        }
        return Arrays.stream(files)
                .collect(Collectors.toList());
    }

    //ls
    public Collection<String> listFiles(String path) throws IOException, FTPDataException {
        path=FileUtil.getParsedPath(path);
        if(path.equals("")){
            path="/";
        }
        FTPFile[] files = ftp.listFiles(path);
        for(FTPFile file: files){
            LOGGER.debug(file);
        }
        return Arrays.stream(files)
                .map(FTPFile::getName)
                .collect(Collectors.toList());
    }

    //ls
    public Collection<String> listFilesSortedByName(String path) throws IOException, FTPDataException {
        path=FileUtil.getParsedPath(path);
        if(path.equals("")){
            path="/";
        }
        FTPFile[] files = ftp.listFiles(path);
        for(FTPFile file: files){
            LOGGER.debug(file);
        }
        return Arrays.stream(files)
                .map(FTPFile::getName)
                .sorted()
                .collect(Collectors.toList());
    }

    //ls
    public Collection<String> listFilesSortedByNameReverse(String path) throws IOException, FTPDataException {
        path=FileUtil.getParsedPath(path);
        if(path.equals("")){
            path="/";
        }
        FTPFile[] files = ftp.listFiles(path);
        for(FTPFile file: files){
            LOGGER.debug(file);
        }
        return Arrays.stream(files)
                .map(FTPFile::getName)
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }


    public List<String> getFileContents(String path, String filename) throws IOException, FTPDataException {
        path=FileUtil.getParsedPath(path);
        if(path.equals("/.") || path.equals(".")){
            path = "";
        }

        if(Path.of(path).getFileName() == null){
            path = path + filename;
        }

        List<String> content = new ArrayList<>();
        InputStream inputStream = ftp.retrieveFileStream(path);

        if(inputStream == null) return new ArrayList<>();

        Scanner fileScanner = new Scanner(inputStream);

        while (fileScanner.hasNextLine()) {
            content.add(fileScanner.nextLine());
        }

        inputStream.close();
        fileScanner.close();

        return content;
    }

    //touch
    //pathWithFilename
    public void touch(String pathWithFilename) throws IOException, FTPDataException {
        pathWithFilename=FileUtil.getParsedPath(pathWithFilename);
        String nameOfFile = FileUtil.getFilenameFromPath(pathWithFilename);
        File myFile = new File(nameOfFile);
        if (myFile.createNewFile()) {
            LOGGER.debug("File created: {}", myFile.getName());
            this.storeFileOnServer(myFile, pathWithFilename);
            myFile.delete();
        } else {
            LOGGER.debug("File already exists.");
        }
    }


    //String pathRemote: To put the file in a directory named Ver, that is locate in the Home "\" directory, write "\Ver"; Write "Ver/fillXY.txt" if you want to put the file in Home dir "\" write "fillXY.txt"
    //client.storeFileOnServer(file, "Ver/fillXY.txt");
    //client.storeFileOnServer(file, "fillXY.txt");
    void storeFileOnServer(File fileToStore, String pathRemoteWithFilename) throws IOException, FTPDataException {
        pathRemoteWithFilename=FileUtil.getParsedPath(pathRemoteWithFilename);
        if(pathRemoteWithFilename.equals("/")||pathRemoteWithFilename.equals("\\")){
            pathRemoteWithFilename = pathRemoteWithFilename+FileUtil.getFilenameFromPath(fileToStore.toString());
        }
        LOGGER.debug("Path: {}", pathRemoteWithFilename);
        FileInputStream inputStream = new FileInputStream(fileToStore);
        ftp.storeFile(pathRemoteWithFilename, inputStream);
        inputStream.close();
    }

    //String pathRemote: To put the file in a directory named Ver, that is locate in the Home "\" directory, write "\Ver"; Write "Ver/fillXY.txt" if you want to put the file in Home dir "\" write "/"
    //client.storeFileOnServer("C:\\Verena_V\\Informatik\\ftp_test_project\\FTP_Test\\temp83907654\\fillXY.txt", "Ver");
    //client.storeFileOnServer("C:\\Verena_V\\Informatik\\ftp_test_project\\FTP_Test\\temp83907654\\fillXY.txt", "/");
    public void storeFileOnServer(String pathToFileToStore, String pathRemoteWithFilename) throws IOException, FTPDataException {
        pathToFileToStore=FileUtil.getParsedPath(pathToFileToStore);
        pathRemoteWithFilename=FileUtil.getParsedPath(pathRemoteWithFilename);
        LOGGER.debug("######## pathToFileToStore: {}", pathToFileToStore);
        LOGGER.debug("######## pathRemote: {}", pathRemoteWithFilename);
        if(pathRemoteWithFilename.equals("/")||pathRemoteWithFilename.equals("\\")){
            pathRemoteWithFilename = pathRemoteWithFilename+FileUtil.getFilenameFromPath(pathToFileToStore);
        }
        LOGGER.debug("######## pathRemote neu: {}", pathRemoteWithFilename);
        FileInputStream inputStream = new FileInputStream(pathToFileToStore);
        ftp.storeFile(pathRemoteWithFilename, inputStream);
        inputStream.close();
    }

    //mkdir
    public void makeDirectory(String pathname) throws IOException, FTPDataException {
        pathname=FileUtil.getParsedPath(pathname);
        ftp.makeDirectory(pathname);
    }

    //mv
    public void rename(String from, String to) throws IOException, FTPDataException {
        from=FileUtil.getParsedPath(from);
        to=FileUtil.getParsedPath(to);
        ftp.rename(from, to);
    }

    //rm
    void remove(String pathWithFilename) throws IOException, FTPDataException {
        pathWithFilename=FileUtil.getParsedPath(pathWithFilename);
        String pathWithoutFilename = FileUtil.getPathWithoutFilename(pathWithFilename);
        LOGGER.debug("pathWithoutFilename: {}", pathWithoutFilename);
        String filename = FileUtil.getFilenameFromPath(pathWithFilename);
        LOGGER.debug("filename: {}", filename);
        FTPFile[] files = ftp.listFiles(pathWithoutFilename);
        for(FTPFile file: files){
            LOGGER.debug("Filename: {}  ###  Typ: {}", file.getName(), file.getType());
            if(file.getName().equals(filename)){
                if(file.getType()==0){
                    removeFile(pathWithFilename);
                }else if(file.getType()==1){
                    removeDirectory(pathWithFilename);
                }
            }
        }
    }

    //rm file
    public void removeFile(String path) throws IOException, FTPDataException {
        path=FileUtil.getParsedPath(path);
        ftp.deleteFile(path);
    }

    //rm directory
    public boolean removeDirectory(String pathname) throws IOException, FTPDataException {
        pathname=FileUtil.getParsedPath(pathname);
        return ftp.removeDirectory(pathname);
    }

    //rm
    public String removeAll(String pathname) throws IOException, FTPDataException {
        String empty = "empty";
        pathname=FileUtil.getParsedPath(pathname);
        FTPFile[] files = ftp.listFiles(pathname);
        if(files.length==0){
            return empty;
        }
        for(FTPFile file: files){
            LOGGER.debug("Filename: {}  ###  Typ: {}",file.getName(), file.getType());
            if(file.getType()==0){
                removeFile(pathname+"/"+file.getName());
            }else if(file.getType()==1){
                String path = removeAll(pathname+"/"+file.getName());
                if(path.equals(empty)){
                    removeDirectory(pathname+"/"+file.getName());
                }else{
                    removeAll(pathname+"/"+file.getName());
                }
            }
        }
        FTPFile[] filesAfter = ftp.listFiles(pathname);
        if(filesAfter.length==0){
            return empty;
        }

        return pathname;
    }

    //cd
    public boolean changeWorkingDirectory(String pathname) throws IOException, FTPDataException {
        pathname=FileUtil.getParsedPath(pathname);
        return changeWorkingDirectory(pathname);
    }

    //pwd
    public String printWorkingDirectory() throws IOException {
        return ftp.printWorkingDirectory();
    }

   void downloadFile(String pathTofileToDownlWithFilename, String pathToDestinationWithoutFilename) throws IOException, FTPDataException {
       pathTofileToDownlWithFilename=FileUtil.getParsedPath(pathTofileToDownlWithFilename);
       pathToDestinationWithoutFilename=FileUtil.getParsedPath(pathToDestinationWithoutFilename);
       FileOutputStream out = new FileOutputStream(pathToDestinationWithoutFilename);
       ftp.retrieveFile(pathTofileToDownlWithFilename, out);
       out.close();
   }

   //read File
   public String readFile(String pathToFileForDownloadWithFilename) throws IOException, FTPDataException {
       pathToFileForDownloadWithFilename=FileUtil.getParsedPath(pathToFileForDownloadWithFilename);
       String nameOfFileToDownload = FileUtil.getFilenameFromPath(pathToFileForDownloadWithFilename);

       File dir = new File(TEMP_FOLDER);
       dir.mkdirs();
       dir.setWritable(true);
       dir.setReadable(true);
       dir.setExecutable(true);

       String destination = dir+"\\"+nameOfFileToDownload;
       downloadFile(pathToFileForDownloadWithFilename, destination);
       List<String> read = FileUtil.readLocalFile(destination);
       String stringRead = String.join("", read);
       FileUtil.deleteTempDir(dir);
       return stringRead;
   }

    //Downloads the specified file in a temp directory (to be written with a local tool) and then sends it
    // back. The local folder and file are then deleted.
    // -> The actual write has to be done bei another function on the now local file
    public void writeFile(String pathToFileToWriteToWithFilename, String message) throws IOException, FTPDataException {
        pathToFileToWriteToWithFilename=FileUtil.getParsedPath(pathToFileToWriteToWithFilename);
        String nameOfFileToDownload = FileUtil.getFilenameFromPath(pathToFileToWriteToWithFilename);
        File dir = new File(TEMP_FOLDER);
        dir.mkdirs();
        dir.setWritable(true);
        dir.setReadable(true);
        dir.setExecutable(true);

        String os = System.getProperty("os.name");
        char folderSeparator='\\';
        if(os.equalsIgnoreCase("Linux")){
            folderSeparator='/';
        }

        String destination = dir.getAbsolutePath()+folderSeparator+nameOfFileToDownload;
        downloadFile(pathToFileToWriteToWithFilename, destination);
        FileUtil.readLocalFile(destination);

        //Write local file
        //++++++ local method ++++++++
        try (FileWriter myWriter = new FileWriter(TEMP_FOLDER+"/"+nameOfFileToDownload);){
            myWriter.write(message);
        }

        //Send modified local file to server
        storeFileOnServer(destination, pathToFileToWriteToWithFilename);
        //Delete local file and temp directory
        FileUtil.deleteTempDir(dir);
    }


    //Shows file meta
    public FTPFile mlistFile(String pathname) throws IOException, FTPDataException {
        pathname=FileUtil.getParsedPath(pathname);
        return ftp.mlistFile(pathname);
    }

    public boolean remoteAppend(String fileName) throws IOException, FTPDataException {
        fileName=FileUtil.getParsedPath(fileName);
        return ftp.remoteAppend(fileName);
    }

    public boolean appendFile(String remote, final InputStream local) throws IOException, FTPDataException {
        remote=FileUtil.getParsedPath(remote);
        return appendFile(remote, local);
    }

}