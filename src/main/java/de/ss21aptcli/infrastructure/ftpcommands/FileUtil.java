package de.ss21aptcli.infrastructure.ftpcommands;

import de.ss21aptcli.infrastructure.FTPDataException;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtil {


    public static String getParsedPath(String cmdlinePath) throws FTPDataException {
        try {

            if(cmdlinePath.isEmpty()|| !cmdlinePath.contains("@")){
                return cmdlinePath;
            }

            String[] parts = cmdlinePath.split("@", 2);

            if (parts[1].isEmpty()|(parts[1].contains("/")&&parts[1].contains("\\"))) {
                throw new FTPDataException("The given path is invalid");
            }

            if (parts[1].contains("/")) {
                String[] backParts = parts[1].split("(?=/)", 2);
                if(backParts[1].endsWith("/")) {
                    int indexLast = backParts[1].lastIndexOf('/');
                    int indexFirst = backParts[1].indexOf('/');
                    int length = backParts[1].length();
                    if (indexLast == (length - 1) && indexFirst != indexLast) {
                        backParts[1] = backParts[1].substring(0, indexLast);
                    }
                }
                return backParts[1];
            }

            return "/";

        }catch (Exception e){
            throw  new FTPDataException("The given path is invalid");
        }
    }

    static String getFilenameFromPath(String pathWithFilename){
        Path path = Paths.get(pathWithFilename);
        System.out.println(path);
        String nameOfFile= path.getFileName().toString();
        System.out.println(nameOfFile);
        return nameOfFile;
    }

    static String getPathWithoutFilename(String pathWithFilename){
        Path path = Paths.get(pathWithFilename);
        //System.out.println(path);
        String pathWithoutFilename="";
        try{
            pathWithoutFilename= path.getParent().toString();
        }catch(NullPointerException e){
            pathWithoutFilename="/";
        }

        //System.out.println("pathWithoutFilename: "+pathWithoutFilename);
        if(pathWithoutFilename.equals("\\")){
            pathWithoutFilename="/";
        }
        return pathWithoutFilename;
    }

    static List<String> readLocalFile(String fileName) throws IOException {
        //System.out.println("##################################################");
        //try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            //stream.forEach(System.out::println);
        //}
        //System.out.println("##################################################");
        Stream<String> fileStream = Files.lines(Paths.get(fileName));   //stream
        List<String> filelist = fileStream.collect(Collectors.toList());    //list
        System.out.println("######### File: "+filelist);
        return filelist;
    }

    public static void deleteTempDir(File dir) throws IOException {
        Path directory = Paths.get(dir.getAbsolutePath());
        Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }
}
