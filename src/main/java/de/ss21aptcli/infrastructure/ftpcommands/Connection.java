package de.ss21aptcli.infrastructure.ftpcommands;

import de.ss21aptcli.domain.models.FTPData;

import java.io.IOException;
import java.net.Inet4Address;

public class Connection {

    public static FtpClient connectInetAdr(int port, String user, String password) throws IOException {
        Inet4Address adr = (Inet4Address) Inet4Address.getLocalHost();
        FtpClient client = new FtpClient(port, user, password, adr);
        client.openWithInetAdress();
        return client;
    }

    public static FtpClient connectInetAdr() throws IOException {
        Inet4Address adr = (Inet4Address) Inet4Address.getLocalHost();
        FtpClient client = new FtpClient(4567, "verena", "1234", adr);
        client.openWithInetAdress();
        return client;
    }

    public static FtpClient connect(String user, String password) throws IOException {
        FtpClient client = new FtpClient("host.docker.internal", 4567, user, password);
        client.open();
        return client;
    }

    public static FtpClient connect() throws IOException {
        FtpClient client = new FtpClient("localhost", 4567, "user", "1234");
        client.open();
        return client;
    }

    public static FtpClient connect(FTPData ftpData) throws IOException {
        FtpClient client = null;
        if (ftpData.getHost().equals("localhost") || ftpData.getHost().equals("127.0.0.1")) {
            client = Connection.connectInetAdr(ftpData.getPort(), ftpData.getUsername(), ftpData.getPassword());
        } else {
            client = Connection.connect(ftpData.getHost(), ftpData.getPort(), ftpData.getUsername(), ftpData.getPassword());
        }
        client.open();
        return client;
    }

    public static FtpClient connect(String server) throws IOException {
        FtpClient client = new FtpClient(server, 4567, "verena", "123");
        client.open();
        return client;
    }

    public static FtpClient connect(String server, int port, String user, String password) throws IOException {
        FtpClient client = new FtpClient(server, port, user, password);
        client.open();
        return client;
    }
}
