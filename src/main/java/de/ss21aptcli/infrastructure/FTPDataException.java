package de.ss21aptcli.infrastructure;

/**
 * Any problem with the credentials of the FTP-Path
 */
public class FTPDataException extends Exception{

    public FTPDataException(String msg){
        super(msg);
    }

}
