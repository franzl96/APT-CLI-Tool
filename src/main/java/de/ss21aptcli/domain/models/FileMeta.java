package de.ss21aptcli.domain.models;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Class witch contains a file meta
 * <p>Can be either a directory or a file
 * <p>Hint: If FileMeta is a Directory isDir is true and content is empty
 * @version 2.0
 * @author Julian, Alexander, Franz
 */
public class FileMeta {

    private List<String> content;
    private boolean isDir;
    private boolean canRead;
    private boolean canWrite;
    private boolean canExecute;
    private String name;
    private String owner;
    private long size;
    private LocalDateTime lastModified;

    public FileMeta() {
    }

    /**
     * Constrictor for content which sets all the variables
     * @param content Content of the file if is dir set null or new List
     * @param isDir If is a directory set true
     * @param name name of the file or folder
     * @param owner owner of the file or folder
     * @param size size of the file or folder
     * @param lastModified last time the file or folder was modified
     */
    public FileMeta(List<String> content, boolean isDir, String name, String owner, long size, LocalDateTime lastModified) {
        this.content = content;
        this.isDir = isDir;
        this.name = name;
        this.owner = owner;
        this.size = size;
        this.lastModified = lastModified;
    }

    /**
     * Returns the content of a file
     * @return is null or empty if {@link FileMeta} is a directory
     */
    public List<String> getContent() {
        return content;
    }

    /**
     * Returns if the {@link FileMeta} is a directory or a file
     * @return True if it is a directory<p> false if it is a file
     */
    public boolean isDir() {
        return isDir;
    }

    public String getName() {
        return name;
    }

    public String getOwner() {
        return owner;
    }

    public long getSize() {
        return size;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    /**
     * Sets the content of the file
     * @param content null or empty list if {@link FileMeta} is a directory
     */
    public void setContent(List<String> content) {
        this.content = content;
    }

    /**
     * Sets if the {@link FileMeta} is a directory
     * @param dir True if it is a directory<p> false if it is a file
     */
    public void setDir(boolean dir) {
        isDir = dir;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public void setCanRead(boolean canRead) {
        this.canRead = canRead;
    }

    public void setCanWrite(boolean canWrite) {
        this.canWrite = canWrite;
    }

    public void setCanExecute(boolean canExecute) {
        this.canExecute = canExecute;
    }

    public boolean canRead() {
        return canRead;
    }

    public boolean canWrite() {
        return canWrite;
    }

    public boolean canExecute() {
        return canExecute;
    }


    @Override
    public String toString() {
        return "FileMeta{" +
                "content=" + content +
                ", isDir=" + isDir +
                ", name='" + name + '\'' +
                ", owner='" + owner + '\'' +
                ", size=" + size +
                ", lastModified=" + lastModified +
                '}';
    }

    /**
     * This Methode checks if the file name equals the other file name and distinguish if the file a directory or a normal file
     * @param obj to check object
     * @return true or false depending on the obj
     */
    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(this == obj) return true;

        FileMeta o2 = (FileMeta) obj;

        if(o2.isDir == this.isDir && o2.name.equals(this.name)) return true; //Ordner mit gleichen Namen sind gleich
        else if(o2.isDir != this.isDir && o2.name.equals(this.name)) return true; //Dateien mit gleichem Namen sind gleich
        else return false; //Datei und Ordner können gleichen Namen haben!

    }
}
