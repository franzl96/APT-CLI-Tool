package de.ss21aptcli.domain.models;

import de.ss21aptcli.infrastructure.FTPDataException;

/**
 * This domain object holds the data for the FTP Connection
 * The object can read the string of the meta and generate the fields
 */
public class FTPData {

    private String username;
    private String password;
    private String host;
    private Integer port;

    /**
     *
     * @param password The password for the FTP connection
     * @param meta The string for the ftp connection - Format: ftp://user@host:port/directory
     * @throws FTPDataException If the meta not readable this exception will be thrown!
     */
    public FTPData(String password, String meta) throws FTPDataException {
        try {
            this.password = password;
            String[] parts = meta.split("ftp://");
            parts = parts[1].split("@", 2);
            this.username = parts[0];
            parts = parts[1].split(":", 2);
            this.host = parts[0];
            parts = parts[1].split("/", 2);
            this.port = Integer.parseInt(parts[0]);
        }catch (Exception e){
            throw  new FTPDataException("The given path is invalid");
        }
    }

    @Override
    public String toString() {
        return "FTPData{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", host='" + host + '\'' +
                ", port='" + port + '\'' +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
