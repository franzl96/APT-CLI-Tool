package de.ss21aptcli.application.commands;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.commands.log.LogCommand;
import de.ss21aptcli.application.commands.log.LogCommandData;
import de.ss21aptcli.application.exceptions.FileNotFound;
import de.ss21aptcli.application.exceptions.PathNotFoundException;
import de.ss21aptcli.domain.models.FileMeta;
import de.ss21aptcli.infrastructure.LocalFileSystemImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

class LogCommandTest {

    private static final Logger LOGGER = LogManager.getLogger(LogCommandTest.class);

    @InjectMocks
    LogCommand command;

    @Mock(name = "fileSystem")
    LocalFileSystemImpl fileSystem;

    FileMeta fileMeta;

    @BeforeEach
    void setUp() throws FileNotFound, PathNotFoundException {

        MockitoAnnotations.openMocks(this);
        List<String> content = new LinkedList<>();
        for (int i = 0; i < 60; i++) {
            content.add("2021-06-15 21:15:16,342 INFO d.s.a.c.LogCommandTest [Test worker] Test"+ i);
            content.add("2021-06-15 21:15:16,342 ERROR d.s.a.c.LogCommandTest [Test worker] Test"+ i);
            content.add("2021-06-15 21:15:16,342 DEBUG d.s.a.c.LogCommandTest [Test worker] Test"+ i);
            content.add("2021-06-15 21:15:16,342 COMMAND d.s.a.c.LogCommandTest [Test worker] Test"+ i);
            content.add("2021-06-15 21:15:16,342 OUT d.s.a.c.LogCommandTest [Test worker] Test"+ i);
        }
        fileMeta = new FileMeta(content,false,"log.log","root",12, LocalDateTime.of(2021,6,15,6,12));
        when(fileSystem.getFile(anyString())).thenReturn(fileMeta);
    }

    @Test
    void testExtendedCommand() throws CommandLineException {
        LogCommandData data = new LogCommandData(true,true,false,false,false);
        List<String> content = new LinkedList<>();
        for (int i = 10; i < 60; i++) {
            content.add("2021-06-15 21:15:16,342 COMMAND d.s.a.c.LogCommandTest [Test worker] Test"+ i);
        }
        assertEquals(content, command.handle(data));
    }

    @Test
    void testExtendedErrors() throws CommandLineException {
        LogCommandData data = new LogCommandData(true,false,true,false,false);
        List<String> content = new LinkedList<>();
        for (int i = 10; i < 60; i++) {
            content.add("2021-06-15 21:15:16,342 ERROR d.s.a.c.LogCommandTest [Test worker] Test"+ i);
        }
        assertEquals(content, command.handle(data));
    }

    @Test
    void testExtendedInfos() throws CommandLineException {
        LogCommandData data = new LogCommandData(true,false,false,true,false);
        List<String> content = new LinkedList<>();
        for (int i = 10; i < 60; i++) {
            content.add("2021-06-15 21:15:16,342 INFO d.s.a.c.LogCommandTest [Test worker] Test"+ i);
        }
        assertEquals(content, command.handle(data));
    }

    @Test
    void testExtendedDebug() throws CommandLineException {
        LogCommandData data = new LogCommandData(true,false,false,false,true);
        List<String> content = new LinkedList<>();
        for (int i = 10; i < 60; i++) {
            content.add("2021-06-15 21:15:16,342 DEBUG d.s.a.c.LogCommandTest [Test worker] Test"+ i);
        }
        assertEquals(content, command.handle(data));
    }

    @Test
    void testExtendedAll() throws CommandLineException {
        LogCommandData data = new LogCommandData(true,true,true,true,true);
        List<String> content = new LinkedList<>();
        content.add("2021-06-15 21:15:16,342 DEBUG d.s.a.c.LogCommandTest [Test worker] Test47");
        content.add("2021-06-15 21:15:16,342 COMMAND d.s.a.c.LogCommandTest [Test worker] Test47");
        for (int i = 48; i < 60; i++) {
            content.add("2021-06-15 21:15:16,342 INFO d.s.a.c.LogCommandTest [Test worker] Test"+ i);
            content.add("2021-06-15 21:15:16,342 ERROR d.s.a.c.LogCommandTest [Test worker] Test"+ i);
            content.add("2021-06-15 21:15:16,342 DEBUG d.s.a.c.LogCommandTest [Test worker] Test"+ i);
            content.add("2021-06-15 21:15:16,342 COMMAND d.s.a.c.LogCommandTest [Test worker] Test"+ i);
        }
        assertEquals(content, command.handle(data));
    }

    @Test
    void testNotExtendedAll() throws CommandLineException {
        LogCommandData data = new LogCommandData(false,true,true,true,true);
        List<String> content = new LinkedList<>();
        content.add("DEBUG Test47");
        content.add("COMMAND Test47");
        for (int i = 48; i < 60; i++) {
            content.add("INFO Test"+ i);
            content.add("ERROR Test"+ i);
            content.add("DEBUG Test"+ i);
            content.add("COMMAND Test"+ i);
        }
        assertEquals(content, command.handle(data));
    }
}