package de.ss21aptcli.commands.mkdir;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.commands.mkdir.MakeDirectoryCommand;
import de.ss21aptcli.application.commands.mkdir.MakeDirectoryCommandData;
import de.ss21aptcli.application.interfaces.IVoidCommand;
import de.ss21aptcli.infrastructure.LocalFileSystemImpl;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class MkdirTest {


    @AfterEach
    public void cleanUpEach() throws IOException {
        FileUtils.deleteDirectory(new File("testFolder"));
    }

    @Test
    void mkdirCurrentPath() throws CommandLineException {

        IVoidCommand<MakeDirectoryCommandData> command = new MakeDirectoryCommand(new LocalFileSystemImpl());

        command.handle(new MakeDirectoryCommandData(false, "testFolder", ""));

        Assertions.assertTrue(Files.exists(Path.of("testFolder")));
    }

}
