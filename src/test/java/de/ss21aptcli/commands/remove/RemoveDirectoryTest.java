package de.ss21aptcli.commands.remove;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.commands.remove.RemoveCommand;
import de.ss21aptcli.application.commands.remove.RemoveCommandData;
import de.ss21aptcli.application.interfaces.IVoidCommand;
import de.ss21aptcli.infrastructure.LocalFileSystemImpl;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class RemoveDirectoryTest {

    @BeforeEach
    public void createDirectory() throws IOException {
        FileUtils.forceMkdir(new File("TestDirRemove/"));
    }

    @Test
    void itShouldDeleteADirectory() throws CommandLineException {

        IVoidCommand<RemoveCommandData> removeDirectory = new RemoveCommand(new LocalFileSystemImpl());

        removeDirectory.handle(new RemoveCommandData("TestDirRemove/",true));

        // Check if the directory is gone
        Assertions.assertTrue(Files.notExists(Path.of("TestDirRemove/")));
    }
}
