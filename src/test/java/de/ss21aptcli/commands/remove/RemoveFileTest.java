package de.ss21aptcli.commands.remove;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.commands.remove.RemoveCommand;
import de.ss21aptcli.application.commands.remove.RemoveCommandData;
import de.ss21aptcli.application.interfaces.IVoidCommand;
import de.ss21aptcli.infrastructure.LocalFileSystemImpl;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class RemoveFileTest {

    @BeforeEach
    public void createFile() throws IOException {
        FileUtils.touch(new File("testFileRemove.txt"));
    }

    @Test
    void itShouldDeleteAFile() throws CommandLineException {

        IVoidCommand<RemoveCommandData> removeFile = new RemoveCommand(new LocalFileSystemImpl());

        removeFile.handle(new RemoveCommandData("testFileRemove.txt",true));

        // Check if the file is gone
        Assertions.assertTrue(Files.notExists(Path.of("testFileRemove.txt")));
    }
}
