package de.ss21aptcli.commands.touch;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.commands.add_new_file.AddNewFileCommand;
import de.ss21aptcli.application.commands.add_new_file.AddNewFileCommandData;
import de.ss21aptcli.application.interfaces.IVoidCommand;
import de.ss21aptcli.infrastructure.LocalFileSystemImpl;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

public class TouchTest {

    @AfterEach
    public void cleanUpEach() {
        FileUtils.deleteQuietly(new File("testFile.txt"));
    }

    @Test
    void addNewFile() throws CommandLineException {

        IVoidCommand<AddNewFileCommandData> command = new AddNewFileCommand(new LocalFileSystemImpl());
        command.handle(new AddNewFileCommandData(false,"testFile.txt", ""));
        Assertions.assertTrue(Files.exists(Path.of("testFile.txt")));
    }


}
