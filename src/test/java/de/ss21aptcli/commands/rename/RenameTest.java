package de.ss21aptcli.commands.rename;

import de.ss21aptcli.application.exceptions.CommandLineException;
import de.ss21aptcli.application.commands.rename.RenameCommand;
import de.ss21aptcli.application.commands.rename.RenameCommandData;
import de.ss21aptcli.application.interfaces.IVoidCommand;
import de.ss21aptcli.infrastructure.LocalFileSystemImpl;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class RenameTest {

    @BeforeEach
    public void createAFile() throws IOException {
        FileUtils.touch(new File("testFileRename.txt"));
    }

    @AfterEach
    public void removeAFile() {
        FileUtils.deleteQuietly(new File("renamedFileRename.txt"));
    }

    @Test
    void itShouldRenameAFile() throws CommandLineException {

        IVoidCommand<RenameCommandData> remove = new RenameCommand(new LocalFileSystemImpl());

        remove.handle(new RenameCommandData("testFileRename.txt", "renamedFileRename.txt"));

        // Check if the renamed file exists
        Assertions.assertTrue(Files.exists(Path.of("renamedFileRename.txt")));
        // Check if the originally file is gone
        Assertions.assertTrue(Files.notExists(Path.of("testFileRename.txt")));
    }
}
