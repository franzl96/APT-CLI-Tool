package de.ss21aptcli.infrastructure;

import de.ss21aptcli.infrastructure.ftpcommands.Connection;
import de.ss21aptcli.infrastructure.ftpcommands.FileUtil;
import de.ss21aptcli.infrastructure.ftpcommands.FtpClient;

import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FtpClientTest {


    FtpClient client;
    File dir;
    File myFile;
    File myFile2;

    @BeforeEach//FtpClient client = connect();
    public void setup() throws IOException, FTPDataException {
        //Connect to server
        client = Connection.connect();
        client.makeDirectory("TestDir");
        //Create Testfolder on Server
        //create file on server root
        dir = new File("./TestLocal");
        dir.mkdirs();
        dir.setWritable(true);
        dir.setReadable(true);
        dir.setExecutable(true);

        myFile = new File("./TestLocal/testPut.txt");
        if (myFile.createNewFile()) {
            System.out.println("File created: " + myFile.getName());
        }

        myFile2 = new File("./TestLocal/testRead.txt");
        if (myFile.createNewFile()) {
            System.out.println("File created: " + myFile.getName());
        }

        client.touch("TestDir/test2.txt");
        client.touch("nosecret.txt");
        client.makeDirectory("TestDir/SubTestDir");

    }


    @AfterEach
    public void teardown() throws IOException, FTPDataException {

        FileUtil. deleteTempDir(dir);
        client.removeAll("/");
        //client.close();

    }

    @AfterAll
    public void close() throws IOException, FTPDataException {
        client.close();

    }

    @Test
    public void createNewFileOnServer() throws IOException, FTPDataException {
        client.touch("nosecret.txt");
        assert client.listFiles().contains("nosecret.txt");
    }

    @Test
    public void createNewFileOnServerLittleVariation() throws IOException, FTPDataException {
        client.touch("/nosecret.txt");
        assert client.listFiles().contains("nosecret.txt");
    }

    @Test
    public void readFilesAndDirectoriesOnServer() throws IOException, FTPDataException {
        //See if Testfolder exists
        assert client.listFiles().contains("TestDir");
    }


    @Test
    public void createNewDirectoryOnServer() throws IOException, FTPDataException {
        client.makeDirectory("TestDir2");
        assert client.listFiles().contains("TestDir2");
    }

    @Test
    public void createNewSubDirectoryOnServer() throws IOException, FTPDataException {
        client.makeDirectory("TestDir2"); //braucht man das vorher?
        client.makeDirectory("TestDir2/SubDir");
        assert client.listFiles("TestDir2").contains("SubDir");
    }

    @Test //client.touch("testFile.txt", "/");
    public void createNewFileInDirectoryOnServer() throws IOException, FTPDataException {
        //See if file exists
        client.touch("/TestDir/testFile.txt");
        client.listFiles("TestDir");
        assert client.listFiles("TestDir").contains("testFile.txt");
    }

    @Test
    public void renameFileOnServer() throws IOException, FTPDataException {
        client.touch("textfile.txt");
        client.rename("textfile.txt", "secret.txt");
        assert client.listFiles().contains("secret.txt");
        assert !client.listFiles().contains("textfile.txt");
    }

    @Test
    public void renameFileInDirectoryOnServer() throws IOException, FTPDataException {
        client.touch("/TestDir/testFile.txt");
        client.rename("/TestDir/testFile.txt", "crazy2.txt");
        assert client.listFiles().contains("crazy2.txt");
        assert !client.listFiles().contains("testFile.txt");
    }


    @Test //copy
    public void storeFileOnServer() throws IOException, FTPDataException {

        client.storeFileOnServer("./TestLocal/testPut.txt", "/");
        assert client.listFiles().contains("testPut.txt");
        //file noch beschreiben und dann auslesen, ob Inhalt auch richtig übertragen wurde

    }


    @Test //copy
    public void storeFileInDirectoryOnServer() throws IOException, FTPDataException {
        client.storeFileOnServer("./TestLocal/testPut.txt", "/TestDir/testPut.txt");
        client.listFiles("TestDir");
        assert client.listFiles("/TestDir").contains("testPut.txt");
    }




    @Test
    public void readFileOnServer() throws IOException, FTPDataException {
        FileWriter myWriter = new FileWriter("./TestLocal/testRead.txt");
        myWriter.write("Some secret message");
        myWriter.close();
        client.storeFileOnServer("./TestLocal/testRead.txt", "/"); //Todo: Funktion vereinfachen
        System.out.println("Read: "+client.readFile("testRead.txt"));
        assert client.readFile("testRead.txt").equals("Some secret message");
    }




    @Test
    public void writeFileInDirectoryOnServer() throws IOException, FTPDataException {
        client.writeFile("nosecret.txt", "Something is writen hier");
        assert client.readFile("nosecret.txt").equals("Something is writen hier");
    }



    @Test
    public void removeFileInDirectoryOnServer() throws IOException, FTPDataException {
        client.touch("/TestDir/test2.txt");
        assert client.listFiles("/TestDir/").contains("test2.txt");
        client.removeFile("/TestDir/test2.txt");
        assert !client.listFiles("/TestDir/").contains("test2.txt");
    }


    @Test
    public void removeDirectoryInDirectoryOnServer() throws IOException, FTPDataException {
        client.makeDirectory("TestDir/SubDir");
        assert client.listFiles("/TestDir/").contains("SubTestDir");
        client.removeDirectory("/TestDir/SubTestDir");
        assert !client.listFiles("/TestDir/").contains("SubTestDir");
    }



    @Test
    public void removeAllInDir() throws IOException, FTPDataException {
        List<String> listFiles = new ArrayList<>(client.listFilesSortedByName("/"));

        assert (listFiles.size()!=0);

        client.removeAll("/");

        List<String> listFiles2 = new ArrayList<>(client.listFilesSortedByName("/"));

        assert (listFiles2.size()==0);

    }


    @Test
    public void lsWithSort() throws IOException, FTPDataException {
        client.removeAll("/");

        client.touch("be.txt");
        client.touch("ce.txt");
        client.touch("ae.txt");
        List<String> listFiles = new ArrayList<>(client.listFilesSortedByName("/"));

        assert listFiles.get(0).equals("ae.txt");
        assert listFiles.get(1).equals("be.txt");
        assert listFiles.get(2).equals("ce.txt");
    }

    @Test
    public void lsWithSortReverse() throws IOException, FTPDataException {
        client.removeAll("/");

        client.touch("be.txt");
        client.touch("ce.txt");
        client.touch("ae.txt");
        List<String> listFiles = new ArrayList<>(client.listFilesSortedByNameReverse("/"));


        assert listFiles.get(2).equals("ae.txt");
        assert listFiles.get(1).equals("be.txt");
        assert listFiles.get(0).equals("ce.txt");


    }



}
