package de.ss21aptcli.infrastructure;

import de.ss21aptcli.infrastructure.ReadFile;
import de.ss21aptcli.infrastructure.WriteFiles;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ReadWriteTest {

    private static File file = new File("Test.txt");

    @BeforeAll
    static void beforeAll() {
        if(file.exists()){
            file.delete();
        }
    }

    @AfterAll
    static void afterAll() {
        if(file.exists()){
            file.delete();
        }
    }

    @Test
    void readTextAsString() {
        String compare = "Lorem ipsum dolor sit amet. Et optio maxime et sapiente dolore eos quis nihil et quibusdam perspiciatis? Eos aliquid iste id velit nisi qui explicabo galisum. Ut quod voluptatem qui rerum doloribus id fugiat quia eum corporis distinctio? Quibusdam quae ut cupiditate accusantium ab expedita animi nam quia quidem non itaque rerum 33 eligendi doloremque ea minima consequatur. Et expedita maxime est debitis recusandae sit nemo tenetur! Vel galisum ipsum qui cupiditate odit aut nisi ratione. Et quisquam iste et dolor labore qui accusamus molestiae aut excepturi cupiditate in incidunt iusto. Ab rerum deleniti est quia asperiores et architecto magni ad illo quisquam sed deserunt assumenda est dolore voluptatibus ab molestiae odit. Ut alias pariatur ut maiores ullam non vitae sapiente.\n" +
                "Sit aliquid atque ea asperiores dignissimos ab explicabo repudiandae aut iure ducimus eos aperiam. Ea magni voluptate est nihil deleniti et odit ratione est quisquam fuga! Ex praesentium eaque et corporis possimus est esse doloribus a molestiae fugit consequatur nisi aut quia quos. Earum quisquam in quia ipsam et soluta sint et molestiae nisi in incidunt adipisci sed natus pariatur. Sit nostrum id consequatur aliquam ut modi aperiam ex odio saepe ut inventore nulla? At libero praesentium et dolorum inventore est itaque voluptate et repellendus blanditiis est placeat molestiae aut nulla voluptate? Ut ullam fuga ut rerum veniam non magni quos id saepe saepe et tempora ipsa qui veniam rerum.\n" +
                "Sed nesciunt eius qui autem optio sit dolor quasi aut nostrum totam in tempora sint quo earum explicabo. Est nulla iusto 33 saepe tenetur et voluptatem id nisi doloribus 33 laboriosam reiciendis et eligendi ducimus et rerum rerum. Qui cumque distinctio ea dolorem iure aut asperiores accusantium. 33 fugit saepe qui ducimus eveniet 33 nobis quam. Ut magni mollitia qui quidem beatae id cupiditate tempore id dolores omnis et deserunt quasi. Et temporibus culpa aut modi natus rem obcaecati sequi ut doloremque iusto! Aut sunt dignissimos non quia corporis et ipsum laborum est consectetur quam quo odit consequatur id atque facere? Vel labore sunt At libero ipsa ut necessitatibus deserunt. Ut alias nihil eos atque blanditiis aut sapiente doloribus sed veniam atque At quos harum quo velit molestias.";
        assertEquals(compare, ReadFile.readFileToString(new File("ReadTest.txt")));
    }

    @Test
    void readFileAsList() {
        List<String> compare = new LinkedList<>();
        compare.add("Lorem ipsum dolor sit amet. Et optio maxime et sapiente dolore eos quis nihil et quibusdam perspiciatis? Eos aliquid iste id velit nisi qui explicabo galisum. Ut quod voluptatem qui rerum doloribus id fugiat quia eum corporis distinctio? Quibusdam quae ut cupiditate accusantium ab expedita animi nam quia quidem non itaque rerum 33 eligendi doloremque ea minima consequatur. Et expedita maxime est debitis recusandae sit nemo tenetur! Vel galisum ipsum qui cupiditate odit aut nisi ratione. Et quisquam iste et dolor labore qui accusamus molestiae aut excepturi cupiditate in incidunt iusto. Ab rerum deleniti est quia asperiores et architecto magni ad illo quisquam sed deserunt assumenda est dolore voluptatibus ab molestiae odit. Ut alias pariatur ut maiores ullam non vitae sapiente.");
        compare.add("Sit aliquid atque ea asperiores dignissimos ab explicabo repudiandae aut iure ducimus eos aperiam. Ea magni voluptate est nihil deleniti et odit ratione est quisquam fuga! Ex praesentium eaque et corporis possimus est esse doloribus a molestiae fugit consequatur nisi aut quia quos. Earum quisquam in quia ipsam et soluta sint et molestiae nisi in incidunt adipisci sed natus pariatur. Sit nostrum id consequatur aliquam ut modi aperiam ex odio saepe ut inventore nulla? At libero praesentium et dolorum inventore est itaque voluptate et repellendus blanditiis est placeat molestiae aut nulla voluptate? Ut ullam fuga ut rerum veniam non magni quos id saepe saepe et tempora ipsa qui veniam rerum.");
        compare.add("Sed nesciunt eius qui autem optio sit dolor quasi aut nostrum totam in tempora sint quo earum explicabo. Est nulla iusto 33 saepe tenetur et voluptatem id nisi doloribus 33 laboriosam reiciendis et eligendi ducimus et rerum rerum. Qui cumque distinctio ea dolorem iure aut asperiores accusantium. 33 fugit saepe qui ducimus eveniet 33 nobis quam. Ut magni mollitia qui quidem beatae id cupiditate tempore id dolores omnis et deserunt quasi. Et temporibus culpa aut modi natus rem obcaecati sequi ut doloremque iusto! Aut sunt dignissimos non quia corporis et ipsum laborum est consectetur quam quo odit consequatur id atque facere? Vel labore sunt At libero ipsa ut necessitatibus deserunt. Ut alias nihil eos atque blanditiis aut sapiente doloribus sed veniam atque At quos harum quo velit molestias.");
        assertEquals(compare,ReadFile.readFile(new File("ReadTest.txt")));
    }

    @Test
    void writeFileWithOverwrite() {
        List<String> compare = new LinkedList<>();
        compare.add("Lorem ipsum dolor sit amet. Et optio maxime et sapiente dolore eos quis nihil et quibusdam perspiciatis? Eos aliquid iste id velit nisi qui explicabo galisum. Ut quod voluptatem qui rerum doloribus id fugiat quia eum corporis distinctio? Quibusdam quae ut cupiditate accusantium ab expedita animi nam quia quidem non itaque rerum 33 eligendi doloremque ea minima consequatur. Et expedita maxime est debitis recusandae sit nemo tenetur! Vel galisum ipsum qui cupiditate odit aut nisi ratione. Et quisquam iste et dolor labore qui accusamus molestiae aut excepturi cupiditate in incidunt iusto. Ab rerum deleniti est quia asperiores et architecto magni ad illo quisquam sed deserunt assumenda est dolore voluptatibus ab molestiae odit. Ut alias pariatur ut maiores ullam non vitae sapiente.");
        compare.add("Sit aliquid atque ea asperiores dignissimos ab explicabo repudiandae aut iure ducimus eos aperiam. Ea magni voluptate est nihil deleniti et odit ratione est quisquam fuga! Ex praesentium eaque et corporis possimus est esse doloribus a molestiae fugit consequatur nisi aut quia quos. Earum quisquam in quia ipsam et soluta sint et molestiae nisi in incidunt adipisci sed natus pariatur. Sit nostrum id consequatur aliquam ut modi aperiam ex odio saepe ut inventore nulla? At libero praesentium et dolorum inventore est itaque voluptate et repellendus blanditiis est placeat molestiae aut nulla voluptate? Ut ullam fuga ut rerum veniam non magni quos id saepe saepe et tempora ipsa qui veniam rerum.");
        compare.add("Sed nesciunt eius qui autem optio sit dolor quasi aut nostrum totam in tempora sint quo earum explicabo. Est nulla iusto 33 saepe tenetur et voluptatem id nisi doloribus 33 laboriosam reiciendis et eligendi ducimus et rerum rerum. Qui cumque distinctio ea dolorem iure aut asperiores accusantium. 33 fugit saepe qui ducimus eveniet 33 nobis quam. Ut magni mollitia qui quidem beatae id cupiditate tempore id dolores omnis et deserunt quasi. Et temporibus culpa aut modi natus rem obcaecati sequi ut doloremque iusto! Aut sunt dignissimos non quia corporis et ipsum laborum est consectetur quam quo odit consequatur id atque facere? Vel labore sunt At libero ipsa ut necessitatibus deserunt. Ut alias nihil eos atque blanditiis aut sapiente doloribus sed veniam atque At quos harum quo velit molestias.");
        WriteFiles.writeFile(file, compare , true);
        assertEquals(compare, ReadFile.readFile(file));
    }

    @Test
    void writeFileWithoutOverwrite() {
        List<String> compare = new LinkedList<>();
        compare.add("Lorem ipsum dolor sit amet. Et optio maxime et sapiente dolore eos quis nihil et quibusdam perspiciatis? Eos aliquid iste id velit nisi qui explicabo galisum. Ut quod voluptatem qui rerum doloribus id fugiat quia eum corporis distinctio? Quibusdam quae ut cupiditate accusantium ab expedita animi nam quia quidem non itaque rerum 33 eligendi doloremque ea minima consequatur. Et expedita maxime est debitis recusandae sit nemo tenetur! Vel galisum ipsum qui cupiditate odit aut nisi ratione. Et quisquam iste et dolor labore qui accusamus molestiae aut excepturi cupiditate in incidunt iusto. Ab rerum deleniti est quia asperiores et architecto magni ad illo quisquam sed deserunt assumenda est dolore voluptatibus ab molestiae odit. Ut alias pariatur ut maiores ullam non vitae sapiente.");
        compare.add("Sit aliquid atque ea asperiores dignissimos ab explicabo repudiandae aut iure ducimus eos aperiam. Ea magni voluptate est nihil deleniti et odit ratione est quisquam fuga! Ex praesentium eaque et corporis possimus est esse doloribus a molestiae fugit consequatur nisi aut quia quos. Earum quisquam in quia ipsam et soluta sint et molestiae nisi in incidunt adipisci sed natus pariatur. Sit nostrum id consequatur aliquam ut modi aperiam ex odio saepe ut inventore nulla? At libero praesentium et dolorum inventore est itaque voluptate et repellendus blanditiis est placeat molestiae aut nulla voluptate? Ut ullam fuga ut rerum veniam non magni quos id saepe saepe et tempora ipsa qui veniam rerum.");
        compare.add("Sed nesciunt eius qui autem optio sit dolor quasi aut nostrum totam in tempora sint quo earum explicabo. Est nulla iusto 33 saepe tenetur et voluptatem id nisi doloribus 33 laboriosam reiciendis et eligendi ducimus et rerum rerum. Qui cumque distinctio ea dolorem iure aut asperiores accusantium. 33 fugit saepe qui ducimus eveniet 33 nobis quam. Ut magni mollitia qui quidem beatae id cupiditate tempore id dolores omnis et deserunt quasi. Et temporibus culpa aut modi natus rem obcaecati sequi ut doloremque iusto! Aut sunt dignissimos non quia corporis et ipsum laborum est consectetur quam quo odit consequatur id atque facere? Vel labore sunt At libero ipsa ut necessitatibus deserunt. Ut alias nihil eos atque blanditiis aut sapiente doloribus sed veniam atque At quos harum quo velit molestias.");
        WriteFiles.writeFile(file, compare , true);
        List<String> newContent = new LinkedList<>();
        newContent.add("Text");
        compare.add("Text");
        WriteFiles.writeFile(file, newContent , false);
        assertEquals(compare, ReadFile.readFile(file));
    }
}