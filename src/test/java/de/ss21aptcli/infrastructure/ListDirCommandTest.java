package de.ss21aptcli.infrastructure;

import de.ss21aptcli.application.commands.list_dir.ListDirCommand;
import de.ss21aptcli.application.commands.list_dir.ListDirCommandData;
import de.ss21aptcli.domain.models.FileMeta;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ListDirCommandTest {

    public FileMeta createDummyFile(){
        List<String> content = new ArrayList<>();
        content.add("Contentid: " + UUID.randomUUID().toString());

        boolean isDir = (new Random().nextInt(2) - 1) == 0;

        long size = new Random().nextInt(1000000);

        LocalDateTime lastModified = LocalDateTime.of(
                new Random().nextInt(2010) + 1,
                new Random().nextInt(11) + 1,
                new Random().nextInt(27) + 1,
                new Random().nextInt(22) + 1,
                new Random().nextInt(58) + 1
        );

        return new FileMeta(content, isDir, UUID.randomUUID().toString(), "Tester", size, lastModified);
    }

    public List<FileMeta> createDummyList(){
        return Stream.generate(this::createDummyFile).limit(30).collect(Collectors.toList());
    }

    public List<FileMeta> createOwnListWithDotFiles(){
        List<FileMeta> files = new ArrayList<>();
        ArrayList<String> content = new ArrayList<String>(){{add("Bla");}};
        files.add(new FileMeta(content, false, "Bart", "Tester", 10000, LocalDateTime.of(2020, 4, 3, 5, 46)));
        files.add(new FileMeta(content, true, "Homer", "Tester", 24235, LocalDateTime.of(2001, 4, 30, 8, 33)));
        files.add(new FileMeta(content, false, "Morty", "Tester", 143141, LocalDateTime.of(2005, 3, 5, 22, 10)));
        files.add(new FileMeta(content, true, "Rick", "Tester", 525413, LocalDateTime.of(2001, 2, 3, 5, 1)));
        files.add(new FileMeta(content, false, ".Blank", "Tester", 4123, LocalDateTime.of(2007, 2, 3, 5, 1)));
        files.add(new FileMeta(content, true, ".Git", "Tester", 2331, LocalDateTime.of(2006, 2, 3, 5, 1)));
        files.add(new FileMeta(content, true, ".", "Tester", 225234, LocalDateTime.of(2000, 2, 3, 5, 1)));
        files.add(new FileMeta(content, true, "..", "Tester", 2345245, LocalDateTime.of(1999, 2, 3, 5, 1)));

        return files;
    }

    public List<FileMeta> createOwnList(){
        List<FileMeta> files = new ArrayList<>();
        ArrayList<String> content = new ArrayList<String>(){{add("Bla");}};
        files.add(new FileMeta(content, false, "Bart", "Tester", 100000, LocalDateTime.of(2020, 4, 3, 5, 46)));
        files.add(new FileMeta(content, true, "Homer", "Tester", 24235, LocalDateTime.of(2001, 4, 30, 8, 33)));
        files.add(new FileMeta(content, false, "Morty", "Tester", 143141, LocalDateTime.of(2005, 3, 5, 22, 10)));
        files.add(new FileMeta(content, true, "Rick", "Tester", 52541, LocalDateTime.of(2001, 2, 3, 5, 1)));

        return files;
    }

    public List<FileMeta> createOwnListOnlyDirs(){
        List<FileMeta> files = new ArrayList<>();
        ArrayList<String> content = new ArrayList<String>(){{add("Bla");}};
        files.add(new FileMeta(content, true, "Homer", "Tester", 24235, LocalDateTime.of(2001, 4, 30, 8, 33)));
        files.add(new FileMeta(content, true, "Rick", "Tester", 52541, LocalDateTime.of(2001, 2, 3, 5, 1)));

        return files;
    }

    @Test
    void command_ls_regularly(){
        ListDirCommand listDirCommand = new ListDirCommand();
        List<FileMeta> content = listDirCommand.modifyList(createOwnListWithDotFiles(), new ListDirCommandData(
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                null
        ));


        content.forEach((System.out::println));
        assertArrayEquals(createOwnList().toArray(), content.toArray());
    }

    @Test
    void command_ls_a(){
        List<FileMeta> ownList = createOwnListWithDotFiles();
        ListDirCommand listDirCommand = new ListDirCommand();
        List<FileMeta> content = listDirCommand.modifyList(ownList, new ListDirCommandData(
                true,
                false,
                false,
                false,
                false,
                false,
                false,
                null
        ));

        content.forEach((System.out::println));
        assertArrayEquals(ownList.toArray(), content.toArray());

    }

    @Test
    void command_almost_all(){
        List<FileMeta> ownList = createOwnListWithDotFiles();
        ListDirCommand listDirCommand = new ListDirCommand();
        List<FileMeta> content = listDirCommand.modifyList(ownList, new ListDirCommandData(
                false,
                true,
                false,
                false,
                false,
                false,
                false,
                null
        ));

        ownList.remove(7);
        ownList.remove(6);

        content.forEach((System.out::println));
        assertArrayEquals(ownList.toArray(), content.toArray());
    }

    @Test
    void command_sizeSort(){
        List<FileMeta> ownList = createOwnList();
        ListDirCommand listDirCommand = new ListDirCommand();
        List<FileMeta> content = listDirCommand.modifyList(ownList, new ListDirCommandData(
                false,
                false,
                true,
                false,
                false,
                false,
                false,
                null
        ));

        content.forEach((System.out::println));
        assertEquals(content.get(0), ownList.get(2));
        assertEquals(content.get(3), ownList.get(1));

    }

    @Test
    void command_timeSort(){
        List<FileMeta> ownList = createOwnList();
        ListDirCommand listDirCommand = new ListDirCommand();
        List<FileMeta> content = listDirCommand.modifyList(ownList, new ListDirCommandData(
                false,
                false,
                false,
                true,
                false,
                false,
                false,
                null
        ));

        content.forEach((System.out::println));
        assertEquals(content.get(0), ownList.get(3));
        assertEquals(content.get(3), ownList.get(0));

    }

    @Test
    void command_ReverseSort(){
        List<FileMeta> ownList = createOwnList();
        ListDirCommand listDirCommand = new ListDirCommand();
        List<FileMeta> content = listDirCommand.modifyList(ownList, new ListDirCommandData(
                false,
                false,
                false,
                false,
                true,
                false,
                false,
                null
        ));

        content.forEach((System.out::println));
        assertEquals(content.get(0), ownList.get(3));
        assertEquals(content.get(1), ownList.get(2));
        assertEquals(content.get(2), ownList.get(1));
        assertEquals(content.get(3), ownList.get(0));

    }

    @Test
    void command_DirectorySort(){
        List<FileMeta> ownList = createOwnList();
        ListDirCommand listDirCommand = new ListDirCommand();
        List<FileMeta> content = listDirCommand.modifyList(ownList, new ListDirCommandData(
                false,
                false,
                false,
                false,
                false,
                true,
                false,
                null
        ));

        content.forEach((System.out::println));
        assertEquals(content.get(0), ownList.get(3));
        assertEquals(content.get(1), ownList.get(1));

    }

    @Test
    void command_DirectoryOnly(){
        ListDirCommand listDirCommand = new ListDirCommand();
        List<FileMeta> content = listDirCommand.modifyList(createOwnListWithDotFiles(), new ListDirCommandData(
                false,
                false,
                false,
                false,
                false,
                false,
                true,
                null
        ));

        content.forEach((System.out::println));
        List<FileMeta> ownList = createOwnListOnlyDirs();
        assertArrayEquals(ownList.toArray(), content.toArray());
    }
}