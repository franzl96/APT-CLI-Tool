package de.ss21aptcli.infrastructure;

import de.ss21aptcli.infrastructure.ftpcommands.FileUtil;
import org.junit.jupiter.api.Test;


public class FileUtilTest {


    @Test
    public void shouldParseCommandLineInputToPath1() throws FTPDataException {
        //ftp://franz@localhost:4567/DirName/filename/anotherfile
        assert FileUtil.getParsedPath("ftp://franz@localhost:4567").equals("/");
    }

    @Test
    public void shouldParseCommandLineInputToPath2() throws FTPDataException {
        //ftp://franz@localhost:4567/DirName/filename/anotherfile
        assert FileUtil.getParsedPath("ftp://franz@localhost:4567/").equals("/");
    }

    @Test
    public void shouldParseCommandLineInputToPath3() throws FTPDataException {
        //ftp://franz@localhost:4567/DirName/filename/anotherfile
        assert FileUtil.getParsedPath("ftp://franz@localhost:4567/DirName").equals("/DirName");
    }

    @Test
    public void shouldParseCommandLineInputToPath4() throws FTPDataException {
        //ftp://franz@localhost:4567/DirName/filename/anotherfile
        assert FileUtil.getParsedPath("ftp://franz@localhost:4567/DirName/filename/").equals("/DirName/filename");
    }

    @Test
    public void shouldParseCommandLineInputToPath5() throws FTPDataException {
        //ftp://franz@localhost:4567/DirName/filename/anotherfile
        assert FileUtil.getParsedPath("ftp://franz@localhost:4567/DirName/filename").equals("/DirName/filename");
    }

    @Test
    public void shouldParseCommandLineInputToPath6() throws FTPDataException {
        //ftp://franz@localhost:4567/DirName/filename/anotherfile
        assert FileUtil.getParsedPath("ftp://franz@localhost:4567/DirName/filename/anotherfile").equals("/DirName/filename/anotherfile");
    }

    /*
    @Test(expected = FTPDataException.class)
    public void TestIfCommandLineInputIsValid() throws FTPDataException {
        //ftp://franz@localhost:4567/DirName/filename/anotherfile
        FileUtil.getParsedPath("ftp://franz@localhost:4567\\DirName/filename/anotherfile");

    }

     */
}