package de.ss21aptcli.infrastructure;

import de.ss21aptcli.infrastructure.ftpcommands.Connection;
import de.ss21aptcli.infrastructure.ftpcommands.FtpClient;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FtpConnectionTest {


    FtpClient client;


    @BeforeAll
    public void setup() throws IOException, FTPDataException {
        client = Connection.connect();
        client.removeAll("/");
        client.touch("test3.txt");
    }

    @AfterAll
    public void teardown() throws IOException, FTPDataException {
        client.removeFile("/test3.txt");
        client.removeAll("/");
        client.close();
    }

    @Test
    public void connectShouldEstablishConnectionWithFTPServer() throws IOException, FTPDataException {
        assert client.listFiles("/").contains("test3.txt");
    }

}
